<?php

return [
    'dashboard' => 'Dashboard',
    'panel' => 'Manage Panel',
    'commerce' => 'eCommerce',

    'upcoming' => 'Upcoming',

    'product' => 'Products',
    'products' => 'All Products',
    'product-add' => 'Add New Product',
    'product-import' => 'Import Product',
    'print-label' => 'Label Print',
    'product-adjust' => 'Quantity Adjustment',
    'product-damage' => 'Damage Product',
    'product-count' => 'Count Product',

    'sale' => 'Sale',
    'sales' => 'All Sale',
    'sale-add' => 'New Sale',
    'sale-pos' => 'POS Sale',
    'sale-shop' => 'Shop Sale',
    'sale-van' => 'Van Sale',

    'purchase' => 'Purchase',
    'purchases' => 'All Purchase',
    'purchase-add' => 'Add New Purchase',
    'purchase-payment' => 'Purchase Payment',
    'purchase-pending' => 'Pending Purchase',
    'purchase-shipping' => 'Shipping Purchase',

    'setup' => 'Setup',

    'setting' => 'Setting',
    'general' => 'General',
    'system' => 'System',
    'price-group' => 'Price Group',
    'warehouse' => 'Warehouse',
    'supplier' => 'Supplier',
    'customer' => 'Customer',
    'staff' => 'Staff',
    'bank' => 'Bank',
    'category' => 'Category',
    'brand' => 'Brand',
    'unit' => 'Unit',


    'language' => 'Language',
    'bangla' => 'Bangla',
    'english' => 'English',
];
