<?php
return [
    'dashboard' => 'ড্যাসবোর্ড',
    'panel' => 'প্যানেল মেনেজ',
    'commerce' => 'ই কমার্স',

    'upcoming' => 'আসন্ন',

    'product' => 'পণ্য',
    'products' => 'সকল পণ্য তালিকা',
    'product-add' => 'নতুন পণ্য যোগ',
    'product-import' => 'অধিক পণ্য ইম্পুর্ট',
    'print-label' => 'লেবেল প্রিন্ট',
    'product-adjust' => 'পণ্য পরিমাপ সমন্বয়',
    'product-damage' => 'খতিগ্রস্ত পণ্য',
    'product-count' => 'পণ্য গননা',

    'sale' => 'বিক্রয়',
    'sales' => 'সকল বিক্রয়',
    'sale-add' => 'নতুন বিক্রয়',
    'sale-pos' => 'পিওএস বিক্রয়',
    'sale-shop' => 'সপ বিক্রয়',
    'sale-site' => 'ইকমার্স বিক্রয়',

    'purchase' => 'ক্রয়',
    'purchases' => 'সকল ক্রয়',
    'purchase-add' => 'নতুন ক্রয়',
    'purchase-payment' => 'ক্রয় পরিশোধ',
    'purchase-pending' => 'অনিষ্পন্ন ক্রয়',
    'purchase-shipping' => 'সিপিং ক্রয়',

    'setup' => 'সেটআপ',

    'setting' => 'ব্যবস্তাপনা',
    'general' => 'সাধারন',
    'system' => 'সিস্টেম',
    'price-group' => 'মুল্য গ্রুপ',
    'warehouse' => 'গুদাম',
    'supplier' => 'সর্বরাহকারি',
    'customer' => 'ক্রেতা',
    'staff' => 'কর্মী',
    'bank' => 'ব্যংক',
    'category' => 'ক্যাটাগরি',
    'brand' => 'ব্রেন্ড',
    'unit' => 'একক',

    'language' => 'ভাষা',
    'bangla' => 'বাংলা',
    'english' => 'ইংরেজি',
];

