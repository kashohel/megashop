import Dashboard from "./components/Dashboard.vue";
import Profile from "./components/Profile.vue";
import Example from "./components/Example";
import FourZeroFour from "./components/FourZeroFour";

export let routes = [
    {path: '/', component: Dashboard},
    {path: '/dashboard', component: Dashboard},
    {path: '/profile', component: Profile},
    {
        path: '/panel', component: Dashboard,
        children: [
            {path: 'link1', component: Example/*,  (a meta field) meta: {requiresAuth: true} */},
            {path: 'profile', component: Profile},
        ]
    },
    {
        path: '/admin', component: Dashboard, meta: {requiresAuth: true},
        children: [
            {path: 'link1', component: Example/*,  (a meta field) meta: {requiresAuth: true} */},
            {path: '/profile', component: Profile},
        ]
    },
    { path: "*", component: FourZeroFour }
    ];
