<li class="nav-item">
    <a href="#">
        <i class="feather icon-home"></i>
        <span class="menu-title" data-i18n="Dashboard">{{ __('menu.dashboard') }}</span>
        <span class="badge badge badge-warning badge-pill float-right mr-2">2</span>
    </a>
    <ul class="menu-content">
        <li><a href="{{ route('panel-dashboard') }}"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Analytics">{{ __('menu.panel') }}</span></a></li>
        <li><a href="{{ route('panel-shop') }}"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="eCommerce">{{ __('menu.commerce') }}</span></a></li>
    </ul>
</li>
<li class="navigation-header"><span>{{ __('menu.upcoming') }}</span></li>
<li class="nav-item has-sub">
    <a href="#"><i class="feather icon-box"></i><span class="menu-title" data-i18n="Extra Components">{{ __('menu.product') }}</span></a>
    <ul class="menu-content">
        <li><a href="{{ route('product.index') }}"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Avatar">{{ __('menu.products') }}</span></a></li>
        <li><a href="{{ route('product.create') }}"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Chips">{{ __('menu.product-add') }}</span></a></li>
        <li><a href="{{ route('product.index') }}"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Divider">{{ __('menu.product-import') }}</span></a></li>
        <li><a href="{{ route('panel-dashboard') }}"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Divider">{{ __('menu.print-label') }}</span></a></li>
        <li><a href="{{ route('panel-dashboard') }}"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Divider">{{ __('menu.product-adjust') }}</span></a></li>
        <li><a href="{{ route('panel-dashboard') }}"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Divider">{{ __('menu.product-damage') }}</span></a></li>
        <li><a href="{{ route('panel-dashboard') }}"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Divider">{{ __('menu.product-count') }}</span></a></li>
    </ul>
</li>
<li class="nav-item has-sub">
    <a href="#"><i class="feather icon-box"></i><span class="menu-title" data-i18n="Extra Components">{{ __('menu.sale') }}</span></a>
    <ul class="menu-content">
        <li><a href="#"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Avatar">{{ __('menu.sales') }}</span></a></li>
        <li><a href="#"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Chips">{{ __('menu.sale-add') }}</span></a></li>
        <li><a href="#"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Divider">{{ __('menu.sale-pos') }}</span></a></li>
        <li><a href="#"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Divider">{{ __('menu.sale-shop') }}</span></a></li>
        <li><a href="#"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Divider">{{ __('menu.sale-site') }}</span></a></li>
    </ul>
</li>
<li class="nav-item has-sub">
    <a href="#"><i class="feather icon-box"></i><span class="menu-title" data-i18n="Extra Components">{{ __('menu.purchase') }}</span></a>
    <ul class="menu-content">
        <li><a href="#"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Avatar">{{ __('menu.purchases') }}</span></a></li>
        <li><a href="#"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Chips">{{ __('menu.purchase-add') }}</span></a></li>
        <li><a href="#"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Divider">{{ __('menu.purchase-payment') }}</span></a></li>
        <li><a href="#"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Divider">{{ __('menu.purchase-pending') }}</span></a></li>
        <li><a href="#"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Divider">{{ __('menu.purchase-shipping') }}</span></a></li>
    </ul>
</li>
<li class="nav-item has-sub">
    <a href="#"><i class="feather icon-box"></i><span class="menu-title" data-i18n="Extra Components">Transfer</span></a>
    <ul class="menu-content">
        <li><a href="#"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Avatar">All Transfer</span></a></li>
        <li><a href="#"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Chips">New Transfer</span></a></li>
    </ul>
</li>
<li class="nav-item has-sub">
    <a href="#"><i class="feather icon-box"></i><span class="menu-title" data-i18n="Extra Components">Return</span></a>
    <ul class="menu-content">
        <li><a href="#"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Avatar">All Returns</span></a></li>
        <li><a href="#"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Chips">New Purchase Return</span></a></li>
        <li><a href="#"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Chips">New Sale Return</span></a></li>
        <li><a href="#"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Chips">Return Fixing</span></a></li>
    </ul>
</li>
<li class="nav-item has-sub">
    <a href="#"><i class="feather icon-shopping-cart"></i><span class="menu-title" data-i18n="Ecommerce">Ecommerce</span></a>
    <ul class="menu-content">
        <li>
            <a href="#"><i
                    class="feather icon-circle"></i><span class="menu-item" data-i18n="Shop">Shop</span></a>
        </li>
        <li>
            <a href="#"><i
                    class="feather icon-circle"></i><span class="menu-item"
                                                          data-i18n="Wish List">Wish List</span></a>
        </li>
        <li>
            <a href="#"><i
                    class="feather icon-circle"></i><span class="menu-item"
                                                          data-i18n="Checkout">Checkout</span></a>
        </li>
    </ul>
</li>
<li class="navigation-header"><span>{{ __('menu.setup') }}</span></li>

<li class="nav-item has-sub">
    <a href="#"><i class="feather icon-settings"></i><span class="menu-title" data-i18n="Components">{{ __('menu.setting') }}</span></a>
    <ul class="menu-content">
        <li>
            <a href="#"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="General">{{ __('menu.general') }}</span></a>
        </li>
        <li>
            <a href="#"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="System">{{ __('menu.system') }}</span></a>
        </li>
        <li>
            <a href="{{ route('priceGroup.index') }}"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="priceGroup">{{ __('menu.price-group') }}</span></a>
        </li>
        <li>
            <a href="{{ route('warehouse.index') }}"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Warehouse">{{ __('menu.warehouse') }}</span></a>
        </li>
        <li>
            <a href="{{ route('supplier.index') }}"><i class="feather icon-circle"></i><span class="menu-item"  data-i18n="Supplier">{{ __('menu.supplier') }}</span></a>
        </li>
        <li>
            <a href="{{ route('customer.index') }}"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Customer">{{ __('menu.customer') }}</span></a>
        </li>
        <li>
            <a href="{{ route('staff.index') }}"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Staff">{{ __('menu.staff') }}</span></a>
        </li>
        <li>
            <a href="{{ route('bank.index') }}"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Bank">{{ __('menu.bank') }}</span></a>
        </li>
        <li>
            <a href="{{ route('category.index') }}"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Category">{{ __('menu.category') }}</span></a>
        </li>
        <li>
            <a href="{{ route('brand.index') }}"><i
                    class="feather icon-circle"></i><span class="menu-item" data-i18n="Brand">{{ __('menu.brand') }}</span></a>
        </li>
        <li>
            <a href="{{ route('unit.index') }}"><i
                    class="feather icon-circle"></i><span class="menu-item" data-i18n="Units">{{ __('menu.unit') }}</span></a>
        </li>
    </ul>
</li>

<li class="nav-item has-sub">
    <a href="#"><i class="feather icon-layout"></i><span class="menu-title" data-i18n="Content">Content</span></a>
    <ul class="menu-content">
        <li><a href="#"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Grid">Grid</span></a></li>
        <li><a href="#"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Typography">Typography</span></a></li>
        <li><a href="#"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Text Utilities">Text Utilities</span></a></li>
        <li><a href="#"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Syntax Highlighter">Syntax Highlighter</span></a></li>
        <li><a href="#"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Helper Classes">Helper Classes</span></a></li>
    </ul>
</li>
<li class="nav-item"><a href="#"><i class="feather icon-droplet"></i><span class="menu-title" data-i18n="Colors">Colors</span></a></li>
<li class="nav-item has-sub">
    <a href="#"><i class="feather icon-eye"></i><span class="menu-title" data-i18n="Icons">Icons</span></a>
    <ul class="menu-content">
        <li>
            <a href="#"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Feather">Feather</span></a>
        </li>
        <li>
            <a href="#"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Font Awesome">Font Awesome</span></a>
        </li>
    </ul>
</li>
<li class="nav-item has-sub">
    <a href="#"><i class="feather icon-credit-card"></i><span class="menu-title" data-i18n="Card">Card</span></a>
    <ul class="menu-content">
        <li>
            <a href="#"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Basic">Basic</span></a>
        </li>
        <li>
            <a href="#"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Advance">Advance</span></a>
        </li>
        <li>
            <a href="#"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Statistics">Statistics</span></a>
        </li>
        <li>
            <a href="#"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Analytics">Analytics</span></a>
        </li>
        <li>
            <a href="#"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Card Actions">Card Actions</span></a>
        </li>
    </ul>
</li>
<li class="nav-item"><a href="#"><i class="feather icon-mail"></i><span class="menu-title" data-i18n="Email">Email</span></a></li>
<li class="nav-item"><a href="#"><i class="feather icon-message-square"></i><span class="menu-title" data-i18n="Chat">Chat</span></a></li>
<li class="nav-item"><a href="#"><i class="feather icon-check-square"></i><span class="menu-title" data-i18n="Todo">Todo</span></a></li>
<li class="nav-item"><a href="#"><i class="feather icon-calendar"></i><span class="menu-title" data-i18n="Calender">Calender</span></a></li>


<li class="nav-item has-sub">
    <a href="#">
        <i class="feather icon-list"></i><span class="menu-title" data-i18n="Data List">Data List</span>
        <span class="badge badge badge-primary badge-pill float-right mr-2">New</span>
    </a>
    <ul class="menu-content">
        <li><a href="#"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="List View">List View</span></a></li>
        <li><a href="#"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Thumb View">Thumb View</span></a></li>
    </ul>
</li>


////////////////////////////////////////////////////////////////




<li class="navigation-header"><span>UI Elements</span></li>

<li class="nav-item"><a href="#"><i class="feather icon-mail"></i><span class="menu-title" data-i18n="Email">Email</span></a></li>
<li class="nav-item"><a href="#"><i class="feather icon-message-square"></i><span class="menu-title" data-i18n="Chat">Chat</span></a></li>
<li class="nav-item"><a href="#"><i class="feather icon-check-square"></i><span class="menu-title" data-i18n="Todo">Todo</span></a></li>
<li class="nav-item"><a href="#"><i class="feather icon-calendar"></i><span class="menu-title" data-i18n="Calender">Calender</span></a></li>


<li class="nav-item has-sub">
    <a href="#">
        <i class="feather icon-list"></i><span class="menu-title" data-i18n="Data List">Data List</span>
        <span class="badge badge badge-primary badge-pill float-right mr-2">New</span>
    </a>
    <ul class="menu-content">
        <li><a href="#"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="List View">List View</span></a></li>
        <li><a href="#"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Thumb View">Thumb View</span></a></li>
    </ul>
</li>
<li class="nav-item has-sub">
    <a href="#"><i class="feather icon-layout"></i><span class="menu-title" data-i18n="Content">Content</span></a>
    <ul class="menu-content">
        <li><a href="#"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Grid">Grid</span></a></li>
        <li><a href="#"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Typography">Typography</span></a></li>
        <li><a href="#"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Text Utilities">Text Utilities</span></a></li>
        <li><a href="#"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Syntax Highlighter">Syntax Highlighter</span></a></li>
        <li><a href="#"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Helper Classes">Helper Classes</span></a></li>
    </ul>
</li>
<li class="nav-item"><a href="#"><i class="feather icon-droplet"></i><span class="menu-title" data-i18n="Colors">Colors</span></a></li>
<li class="nav-item has-sub">
    <a href="#"><i class="feather icon-eye"></i><span class="menu-title" data-i18n="Icons">Icons</span></a>
    <ul class="menu-content">
        <li>
            <a href="#"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Feather">Feather</span></a>
        </li>
        <li>
            <a href="#"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Font Awesome">Font Awesome</span></a>
        </li>
    </ul>
</li>
<li class="nav-item has-sub">
    <a href="#"><i class="feather icon-credit-card"></i><span class="menu-title" data-i18n="Card">Card</span></a>
    <ul class="menu-content">
        <li>
            <a href="#"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Basic">Basic</span></a>
        </li>
        <li>
            <a href="#"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Advance">Advance</span></a>
        </li>
        <li>
            <a href="#"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Statistics">Statistics</span></a>
        </li>
        <li>
            <a href="#"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Analytics">Analytics</span></a>
        </li>
        <li>
            <a href="#"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Card Actions">Card Actions</span></a>
        </li>
    </ul>
</li>
<li class="nav-item has-sub">
    <a href="#"><i class="feather icon-briefcase"></i><span class="menu-title" data-i18n="Components">Components</span></a>
    <ul class="menu-content">
        <li>
            <a href="#"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Alerts">Alerts</span></a>
        </li>
        <li>
            <a href="#"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Buttons">Buttons</span></a>
        </li>
        <li>
            <a href="#"><i
                    class="feather icon-circle"></i><span class="menu-item" data-i18n="Breadcrumbs">Breadcrumbs</span></a>
        </li>
        <li>
            <a href="#"><i
                    class="feather icon-circle"></i><span class="menu-item"  data-i18n="Carousel">Carousel</span></a>
        </li>
        <li>
            <a href="#"><i
                    class="feather icon-circle"></i><span class="menu-item" data-i18n="Collapse">Collapse</span></a>
        </li>
        <li>
            <a href="#"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Dropdowns">Dropdowns</span></a>
        </li>
        <li>
            <a href="#"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="List Group">List Group</span></a>
        </li>
        <li>
            <a href="#"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Modals">Modals</span></a>
        </li>
        <li>
            <a href="#"><i
                    class="feather icon-circle"></i><span class="menu-item" data-i18n="Pagination">Pagination</span></a>
        </li>
        <li>
            <a href="#"><i
                    class="feather icon-circle"></i><span class="menu-item" data-i18n="Navs Component">Navs Component</span></a>
        </li>
        <li>
            <a href="#"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Navbar">Navbar</span></a>
        </li>
        <li>
            <a href="#"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Tabs Component">Tabs Component</span></a>
        </li>
        <li>
            <a href="#"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Pills Component">Pills Component</span></a>
        </li>
        <li>
            <a href="#"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Tooltips">Tooltips</span></a>
        </li>
        <li>
            <a href="#"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Popovers">Popovers</span></a>
        </li>
        <li>
            <a href="#"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Badges">Badges</span></a>
        </li>
        <li>
            <a href="#"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Pill Badges">Pill Badges</span></a>
        </li>
        <li>
            <a href="#"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Progress">Progress</span></a>
        </li>
        <li>
            <a href="#"><i class="feather icon-circle"></i><span class="menu-item">Media Objects</span></a>
        </li>
        <li>
            <a href="#"><i class="feather icon-circle"></i><span class="menu-item"  data-i18n="Spinner">Spinner</span></a>
        </li>
        <li>
            <a href="#"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Toasts">Toasts</span></a>
        </li>
    </ul>
</li>
<li class="navigation-header"><span>Forms &amp; Tables</span>
</li>
<li class="nav-item has-sub"><a href="#"><i class="feather icon-copy"></i><span class="menu-title" data-i18n="Form Elements">Form Elements</span></a>
    <ul class="menu-content">
        <li>
            <a href="#"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Select">Select</span></a>
        </li>
        <li>
            <a href="#"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Switch">Switch</span></a>
        </li>
        <li>
            <a href="#"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Checkbox">Checkbox</span></a>
        </li>
        <li>
            <a href="#"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Radio">Radio</span></a>
        </li>
        <li>
            <a href="#"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Input">Input</span></a>
        </li>
        <li>
            <a href="#"><i  class="feather icon-circle"></i><span class="menu-item" data-i18n="Input Groups">Input Groups</span></a>
        </li>
        <li>
            <a href="#"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Number Input">Number Input</span></a>
        </li>
        <li>
            <a href="#"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Textarea">Textarea</span></a>
        </li>
        <li>
            <a href="#"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Date &amp; Time Picker">Date &amp; Time Picker</span></a>
        </li>
    </ul>
</li>
<li class="nav-item"><a
        href="#"><i
            class="feather icon-box"></i><span class="menu-title" data-i18n="Form Layout">Form Layout</span></a>
</li>
<li class="nav-item"><a
        href="#"><i
            class="feather icon-package"></i><span class="menu-title"
                                                   data-i18n="Form Wizard">Form Wizard</span></a>
</li>
<li class="nav-item"><a
        href="#"><i
            class="feather icon-check-circle"></i><span class="menu-title" data-i18n="Form Validation">Form Validation</span></a>
</li>
<li class="nav-item"><a
        href="#"><i
            class="feather icon-server"></i><span class="menu-title" data-i18n="Table">Table</span></a>
</li>

<li class="nav-item"><a href="#"><i class="feather icon-grid"></i><span class="menu-title" data-i18n="Datatable">Datatable</span></a></li>
<li class="navigation-header"><span>pages</span></li>
<li class="nav-item"><a
        href="#"><i
            class="feather icon-user"></i><span class="menu-title" data-i18n="Profile">Profile</span></a>
</li>
<li class="nav-item"><a
        href="#"><i
            class="feather icon-help-circle"></i><span class="menu-title" data-i18n="FAQ">FAQ</span></a>
</li>
<li class="nav-item"><a
        href="#"><i
            class="feather icon-info"></i><span class="menu-title"
                                                data-i18n="Knowledge Base">Knowledge Base</span></a>
</li>
<li class="nav-item"><a
        href="#"><i
            class="feather icon-search"></i><span class="menu-title" data-i18n="Search">Search</span></a>
</li>
<li class="nav-item"><a
        href="#"><i
            class="feather icon-file"></i><span class="menu-title" data-i18n="Invoice">Invoice</span></a>
</li>
<li class="nav-item has-sub">
    <a href="#"><i class="feather icon-zap"></i><span class="menu-title" data-i18n="Starter kit">Starter kit</span></a>
    <ul class="menu-content">
        <li>
            <a href="#"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="1 column">1 column</span></a>
        </li>
        <li>
            <a href="#"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="2 columns">2 columns</span></a>
        </li>
        <li>
            <a href="#"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Fixed navbar">Fixed navbar</span></a>
        </li>
        <li>
            <a href="#"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Floating navbar">Floating navbar</span></a>
        </li>
        <li>
            <a href="#"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Fixed layout">Fixed layout</span></a>
        </li>
        <li>
            <a href="#"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Static layout">Static layout</span></a>
        </li>
    </ul>
</li>
<li class="nav-item has-sub"><a
        href="#"><i
            class="feather icon-unlock"></i><span class="menu-title" data-i18n="Authentication">Authentication</span></a>
    <ul class="menu-content">
        <li>
            <a href="#"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Login">Login</span></a>
        </li>
        <li>
            <a href=""><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Register">Register</span></a>
        </li>
        <li>
            <a href="#"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Forgot Password">Forgot Password</span></a>
        </li>
        <li>
            <a href="#"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Reset Password">Reset Password</span></a>
        </li>
        <li>
            <a href="#"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Lock Screen">Lock Screen</span></a>
        </li>
    </ul>
</li>
<li class="nav-item has-sub">
    <a href="#"><i class="feather icon-file-text"></i><span class="menu-title" data-i18n="Miscellaneous">Miscellaneous</span></a>
    <ul class="menu-content">
        <li>
            <a href="#"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Coming Soon">Coming Soon</span></a>
        </li>
        <li class="has-sub">
            <a href="#"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Error">Error</span></a>
            <ul class="menu-content">
                <li>
                    <a href="#"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="404">404</span></a>
                </li>
                <li>
                    <a href="#"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="500">500</span></a>
                </li>
            </ul>
        </li>
        <li>
            <a href="#"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Not Authorized">Not Authorized</span></a>
        </li>
        <li>
            <a href="#"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Maintenance">Maintenance</span></a>
        </li>
    </ul>
</li>
<li class="navigation-header"><span>Charts &amp; Maps</span>
</li>
<li class="nav-item has-sub">
    <a href="#">
        <i class="feather icon-pie-chart"></i><span class="menu-title" data-i18n="Charts">Charts</span>
        <span class="badge badge badge-pill badge-success float-right mr-2">3</span>
    </a>
    <ul class="menu-content">
        <li>
            <a href="#"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Apex">Apex</span></a>
        </li>
        <li>
            <a href="#"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Chartjs">Chartjs</span></a>
        </li>
        <li>
            <a href="#"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Echarts">Echarts</span></a>
        </li>
    </ul>
</li>
<li class="nav-item">
    <a href="#"><i class="feather icon-map"></i><span class="menu-title" data-i18n="Google Maps">Google Maps</span></a>
</li>
<li class="navigation-header"><span>Extensions</span></li>
<li class="nav-item">
    <a href="#"><i class="feather icon-alert-circle"></i><span class="menu-title" data-i18n="Sweet Alert">Sweet Alert</span></a>
</li>
<li class="nav-item">
    <a href="#"><i class="feather icon-zap"></i><span class="menu-title" data-i18n="Toastr">Toastr</span></a>
</li>
<li class="nav-item">
    <a href="#"><i class="feather icon-sliders"></i><span class="menu-title" data-i18n="NoUi Slider">NoUi Slider</span></a>
</li>
<li class="nav-item">
    <a href="#"><i class="feather icon-upload-cloud"></i><span class="menu-title" data-i18n="File Uploader">File Uploader</span></a>
</li>
<li class="nav-item">
    <a href="#"><i class="feather icon-edit"></i><span class="menu-title" data-i18n="Quill Editor">Quill Editor</span></a>
</li>
<li class="nav-item">
    <a href="#"><i class="feather icon-droplet"></i><span class="menu-title" data-i18n="Drag &amp; Drop">Drag &amp; Drop</span></a>
</li>
<li class="nav-item">
    <a href="#"><i class="feather icon-info"></i><span class="menu-title" data-i18n="Tour">Tour</span></a>
</li>
<li class="nav-item">
    <a href="#"><i class="feather icon-copy"></i><span class="menu-title" data-i18n="Clipboard">Clipboard</span></a>
</li>
<li class="nav-item">
    <a href="#"><i class="feather icon-more-horizontal"></i><span class="menu-title" data-i18n="Context Menu">Context Menu</span></a>
</li>
<li class="nav-item">
    <a href="#"><i class="feather icon-globe"></i><span class="menu-title" data-i18n="l18n">l18n</span></a>
</li>
<li class="navigation-header"><span>Others</span>
</li>
<li class="nav-item has-sub">
    <a href="#"><i class="feather icon-menu"></i><span class="menu-title" data-i18n="Menu Levels">Menu Levels</span></a>
    <ul class="menu-content">
        <li>
            <a href="#"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Second Level">Second Level</span></a>
        </li>
        <li class="has-sub">
            <a href="#"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Second Level">Second Level</span></a>
            <ul class="menu-content">
                <li>
                    <a href="#"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Third Level">Third Level</span></a>
                </li>
                <li>
                    <a href="#"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Third Level">Third Level</span></a>
                </li>
            </ul>
        </li>
    </ul>
</li>
<li class="disabled nav-item">
    <a href="#"><i class="feather icon-eye-off"></i><span class="menu-title" data-i18n="Disabled Menu">Disabled Menu</span></a>
</li>
<li class="navigation-header"><span>Support</span>
</li>
<li class="nav-item">
    <a href="#">
        <i class="feather icon-file"></i><span class="menu-title" data-i18n="">Changelog</span>
        <span class="badge badge badge-pill badge-danger float-right">1.0</span>
    </a>
</li>
<li class="nav-item">
    <a href="#"><i class="feather icon-folder"></i><span class="menu-title" data-i18n="Documentation">Documentation</span></a>
</li>
<li class="nav-item">
    <a href="#"><i class="feather icon-life-buoy"></i><span class="menu-title" data-i18n="Raise Support">Raise Support</span></a>
</li>
