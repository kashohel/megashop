<footer class="footer footer-static footer-light">
    <p class="clearfix blue-grey lighten-2 mb-0"><span class="float-md-left d-block d-md-inline-block mt-25">COPYRIGHT  © {{ date("Y") }}<a
                class="text-bold-800 grey darken-2"
                href="http://alam.foxof.org" target="_blank">{{ config('app.name') }}</a>All rights Reserved</span><span
            class="float-md-right d-none d-md-block"><span class="foot" title="Khorshed Alam">K</span>
            <span class="foot" title="Al Hasan">A</span>
            <span class="foot" title="Jibon Chandra Roy">J</span>
            <span class="foot" title="Touhidul Islam">T</span> &amp; Made with<i
                class="feather icon-heart pink"></i></span>
        <button class="btn btn-primary btn-icon scroll-top waves-effect waves-light" type="button"><i
                class="feather icon-arrow-up"></i></button>
    </p>
    <style type="text/css">
        .foot {
            border-radius: 25%;
            cursor: default;
            border: 1px solid #EA5455;
            padding: 0 3px;
        }
    </style>
</footer>
