<div class="content-header row">
    <div class="content-header-left col-md-9 col-12 mb-2">
        <div class="row breadcrumbs-top">
            <div class="col-12">
                @if (isset($data['title']) && !null == $data['title'])
                    <h2 class="content-header-title float-left mb-0">{{ $data['title'] }}</h2>
                @endif
                @if (isset($data['breadcrumb']) && !null == $data['breadcrumb'])
                        <div class="breadcrumb-wrapper col-12">
                            <ol class="breadcrumb">
                                @foreach($data['breadcrumb'] as $step => $stepLink)
                                    <li class="breadcrumb-item">
                                        @if ($stepLink)
                                            <a href="{{ $stepLink }}">{{ $step }}</a>
                                        @else
                                            {{ $step }}
                                        @endif
                                    </li>
                                @endforeach
                            </ol>
                        </div>
                @endif
            </div>
        </div>
    </div>
    @if (isset($data['menu']) && !null == $data['menu'])
        <div class="content-header-right text-md-right col-md-3 col-12 d-md-block d-none">
            <div class="form-group breadcrum-right">
                <div class="dropdown">
                    <button class="btn-icon btn btn-primary btn-round btn-sm dropdown-toggle waves-effect waves-light"
                            type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i
                            class="feather icon-settings"></i></button>
                    <div class="dropdown-menu dropdown-menu-right" x-placement="top-end">
                        @foreach($data['menu'] as $menu => $menuLink)
                            <a class="dropdown-item" href="{{ $menuLink }}">{{ $menu }}</a>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    @endif
</div>

{{--@php($topic = ['title' => 'Warehouse List', 'breadcrumb' => ['Dashboard' => route('panel-dashboard'), 'Setting' => route('warehouse.index'), 'Warehouse' => null], 'menu' => ['Add new' => 'addnew', 'Edit' => '', 'Delete' => '']])--}}
