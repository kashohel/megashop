<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>{{ "Welcome to ".config('app.name') }}</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <script src="https://code.jquery.com/jquery-3.4.1.min.js" type="text/javascript"></script>
        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > div {
                color: #636b6f;
                font-size: 13px;
                padding: 0;
                font-weight: 600;
                text-decoration: none;
                display: inline-grid;
            }
            .links > div > a {
                padding: 0 30px;
                margin-bottom: 40px;
            }
            .links .name, .links .job {
                display: none;
            }
            .links > div > h4 {
                padding: 0;
                margin: -38px 0 0 0;
                text-transform: uppercase;
            }

            .links > div > h5 {
                padding: 0;
                margin: -25px 0 0 0;
            }

            .m-b-md {
                margin-bottom: 30px;
            }

            .dev-avatar{
                width: 60px;
                height: 60px;
                border: solid gray;
                border-radius: 50%;
            }

            .sup-avatar{
                width: 100px;
                height: 100px;
                border: solid green;
                border-radius: 50%;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            <div class="top-right links">
            @if(Auth::guard('staff')->check())
                    <a href="{{ url('/panel') }}">Panel</a>
            @else
                    <a href="{{ route('login') }}">Login</a>
            @endif
            </div>

            <div class="content">
                <div class="title m-b-md">
                    {{ config('app.name') }}
                </div>

                <div class="links">
                    <div>
                        <a href="#">
                            <img class="dev-avatar" src="{{ asset('Vuesax/img/devs/Hasan.jpg') }}" alt="">
                        </a>
                        <h4 class="name">Al Hasan</h4>
                        <h5 class="job">(Analyzer)</h5>
                    </div>
                    <div>
                        <a href="#">
                            <img class="dev-avatar" src="{{ asset('Vuesax/img/devs/Jibon.jpg') }}" alt="">
                        </a>
                        <h4 class="name">Jibon Chondro</h4>
                        <h5 class="job">(Database Designer)</h5>
                    </div>
                    <div>
                        <a href="#">
                            <img class="sup-avatar" src="{{ asset('Vuesax/img/devs/AhsanArif.jpg') }}" alt="">
                        </a>
                        <h4 class="name">Ahsan Arif</h4>
                        <h5 class="job">(Supervised by)</h5>
                    </div>
                    <div>
                        <a href="#">
                            <img class="dev-avatar" src="{{ asset('Vuesax/img/devs/Toufiq.jpg') }}" alt="">
                        </a>
                        <h4 class="name">Touhidul Islam</h4>
                        <h5 class="job">(Frontend Designer)</h5>
                    </div>
                    <div>
                        <a href="#">
                            <img class="dev-avatar" src="{{ asset('Vuesax/img/devs/Khorshed.jpg') }}" alt="">
                        </a>
                        <h4 class="name">Khorshed Alam</h4>
                        <h5 class="job">(Backend Coder)</h5>
                    </div>
                </div>
            </div>
        </div>
    </body>
<script>
    $(document).ready(function(){
        $("img").mouseenter(function(){
            $(this).parent().parent().children('.name').fadeIn();
            $(this).parent().parent().children('.job').show(500);
        });
        $("img").mouseleave(function(){
            $(this).parent().parent().children('.name').fadeOut();
            $(this).parent().parent().children('.job').hide(500);
        });
    });
</script>
</html>
