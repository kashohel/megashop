@extends('Master.Panel')

@section('body')
    {{--    @php($topic = ['title' => 'Warehouse', 'breadcrumb' => ['Dashboard' => route('panel-dashboard'), 'Setting' => route('warehouse.index'), 'Warehouse' => null], 'menu' => ['Add new' => 'addnew', 'Edit' => '', 'Delete' => '']])--}}
    {{--    @include('Elements.TopicHeader', ['data' => $topic])--}}

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="mb-0">Price Group List</h4>
                    <div class="content-header-right text-md-right col-md-3 col-12 d-md-block d-none">
                        <div class="dropdown">
                            <button class="btn-icon btn btn-outline-primary btn-round btn-sm dropdown-toggle waves-effect waves-light" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="feather icon-settings"></i>
                            </button>
                            <div class="dropdown-menu dropdown-menu-right">
                                <a class="dropdown-item" id="create"><i class="feather icon-plus"></i>Add Price Group</a>
                                <a class="dropdown-item" href="#"><i class="feather icon-download"></i>Export as Excel</a>
                                <a class="dropdown-item" href="#"><i class="feather icon-trash-2"></i>Delete</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-content listView">
                    <div class="table-responsive mt-1">
                        <table class="table table-hover-animation mb-0">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>PRICE GROUP NAME</th>
                                <th>PRICE GROUP PRODUCT</th>
                                <th class="form-actions right">ACTION</th>
                            </tr>
                            </thead>
                            <tbody>
                            @php($x = 1)
                            @foreach ($priceGroups as $priceGroup)
                                <tr>
                                    <td>{{ $x++ }}</td>
                                    <td>
                                        <i class="fa fa-circle font-small-3 text-success mr-50"></i>{{ $priceGroup->name }}
                                    </td>
                                    <td>{{ $priceGroup->products->count() }} Products</td>
                                    <td class="form-actions right" style="font-size: medium">
                                        <a href="{{ route('priceGroup.show', $priceGroup->code) }}" data-toggle="tooltip" data-placement="top" title="List VIew" data-original-title="View item">
                                            <i class="ficon feather icon-maximize"></i></a>
                                        <a href="{{ route('priceGroup.show', $priceGroup->code) }}" data-toggle="tooltip" data-placement="top" title="List Edit" data-original-title="Edit List item">
                                            <i class="ficon feather icon-paperclip"></i></a>
                                        <a href="{{ route('priceGroup.edit', $priceGroup->code) }}" data-toggle="tooltip" data-placement="top" title="Edit" data-original-title="Edit item">
                                            <i class="ficon feather icon-edit"></i></a>
                                        <a href="{{ route('priceGroup.destroy', $priceGroup->code) }}" data-toggle="tooltip" data-placement="top" title="Delete" data-original-title="Delete item">
                                            <i class="ficon feather icon-trash"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="card-content create" style="display: none">
                    <div class="card-body">
                        {{ Form::open(['route' => 'priceGroup.store', 'method' => 'post', 'class' => 'form form-horizontal']) }}
                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-6 col-12">
                                    <div class="form-group row">
                                        <div class="col-md-4">
                                            <span>{{ Form::label('name', 'Price Group Name') }}</span>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="form-group position-relative has-icon-left input-divider-left">
                                                {{ Form::text('name', null, ['id' => 'name', 'class' => 'form-control round', 'placeholder' => 'Name of new Group', 'autofocus', 'required']) }}
                                                <div class="form-control-position">
                                                    <i class="feather icon-grid"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-12 text-center">
                                    {!! Form::button('Submit', ['type' => 'submit', 'class' => 'btn btn-primary mr-1 mb-1 waves-effect waves-light']) !!}
                                    {!! Form::button('Reset', ['type' => 'reset', 'class' => 'btn btn-outline-warning mr-1 mb-1 waves-effect waves-light']) !!}
                                    {!! Form::button('Cancel', ['id' => 'cancel', 'class' => 'btn btn-warning mr-1 mb-1 waves-effect waves-light']) !!}
                                </div>
                            </div>
                        </div>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script type="text/javascript">
        $(document).ready(function () {
            $("#create").click(function(){
                $(".create").show();
                $(".listView").hide();
                $(".card-header h4").html("Add new Price Group")
            });
            $("#cancel").click(function(){
                $(".create").hide();
                $(".listView").show();
                $(".card-header h4").html("Price Group List")
            });
        });
    </script>
@stop
