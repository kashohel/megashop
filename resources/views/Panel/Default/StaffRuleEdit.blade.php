@extends('Master.Panel')

@section('body')
    {{--    @php($topic = ['title' => 'Staff rule', 'breadcrumb' => ['Dashboard' => route('panel-dashboard'), 'Setting' => route('staff-rule.index'), 'staff-rule' => null], 'menu' => ['Add new' => 'addnew', 'Edit' => '', 'Delete' => '']])--}}
    {{--    @include('Elements.TopicHeader', ['data' => $topic])--}}

    @php($x = 1)
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="mb-0">Staff Rules Edit</h4>
                    <div class="content-header-right text-md-right col-md-3 col-12 d-md-block d-none">
                        <div class="dropdown">
                            <button
                                class="btn-icon btn btn-outline-primary btn-round btn-sm dropdown-toggle waves-effect waves-light"
                                type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="feather icon-settings"></i>
                            </button>
                            <div class="dropdown-menu dropdown-menu-right">
                                <a href="{{ route('staff-rule.index') }}" class="dropdown-item" id="create"><i class="feather icon-plus"></i>All Staff Rule</a>
                                <a class="dropdown-item" href="#"><i class="feather icon-download"></i>Export as
                                    Excel</a>
                                <a class="dropdown-item" href="#"><i class="feather icon-trash-2"></i>Delete</a>
                            </div>
                        </div>
                    </div>
                </div>
                {{ Form::open(['route' => ['staff-rule.update', $rule->code], 'method' => 'put', 'class' => 'form form-horizontal validate']) }}
                <div class="card-content create">
                    <div class="card-body">
                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-4 col-12">
                                    <div class="form-group row">
                                        <div class="col-md-4">
                                            <span>{{ Form::label('name', 'Rule Name') }}</span>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="form-group position-relative has-icon-left input-divider-left">
                                                {{ Form::text('name', $rule->name, ['id' => 'name', 'class' => 'form-control round', 'placeholder' => 'Warehouse Name', 'required']) }}
                                                <div class="form-control-position">
                                                    <i class="feather icon-grid"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-8 col-12">
                                    <div class="form-group row">
                                        <div class="col-md-2">
                                            <span>{{ Form::label('details', 'Details') }}</span>
                                        </div>
                                        <div class="col-md-10">
                                            <div class="form-group position-relative has-icon-left input-divider-left">
                                                {{ Form::text('details', $rule->details, ['id' => 'phone', 'rows' => 1, 'class' => 'form-control round', 'placeholder' => 'Phone Number']) }}
                                                <div class="form-control-position">
                                                    <i class="feather icon-file-text"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <p class="card-text">By <code>ticking</code>, it means giving permission and canceling by <code>tick
                                out</code>. Complete the appropriate form and click the submit button to save.</p>

                        <div class="row" id="table-bordered">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-content">
                                        <div class="table-responsive">
                                            <table class="table table-bordered mb-0">
                                                <thead class="text-center">
                                                <tr>
                                                    <th rowspan="2">ID</th>
                                                    <th rowspan="2" style="vertical-align:middle">Module Name</th>
                                                    <th colspan="5">Permission</th>
                                                </tr>
                                                <tr>
                                                    <th>View</th>
                                                    <th>Add</th>
                                                    <th>Edit</th>
                                                    <th>Delete</th>
                                                    <th>Miscellaneous</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach ($modules as $module => $permission)
                                                    <tr>
                                                        <th scope="row">{{ $x++ }}</th>
                                                        <td>{{ $module }}</td>
                                                        @if (isset($permission[0]) && !null == $permission[0])
                                                            @foreach($permission[0] as $first[$x] => $crud)
                                                                <td class="align-center">
                                                                    <div class="vs-checkbox-con vs-checkbox-primary">
                                                                        {!! Form::checkbox($crud, '1', $rule->$crud,  ['id' => 'name']) !!}
                                                                        <span class="vs-checkbox"
                                                                              style="margin-right: 0">
                                                            <span class="vs-checkbox--check">
                                                                <i class="vs-icon feather icon-check"></i>
                                                            </span>
                                                        </span>
                                                                    </div>
                                                                    {{--                                                        {!! Form::checkbox('name', '1', null,  ['id' => 'name']) !!}--}}
                                                                </td>
                                                            @endforeach
                                                        <td>
                                                            <ul class="list-unstyled mb-0">
                                                                @foreach ($permission[1] as $misc => $miscName)
                                                                    <li class="d-inline-block mr-2">
                                                                        <fieldset>
                                                                            <div
                                                                                class="vs-checkbox-con vs-checkbox-primary">
                                                                                {!! Form::checkbox($misc, '1', $rule->$misc,  ['id' => 'name']) !!}
                                                                                <span class="vs-checkbox">
                                                                                    <span class="vs-checkbox--check">
                                                                                        <i class="vs-icon feather icon-check"></i>
                                                                                    </span>
                                                                                </span>
                                                                                <span class="">{{ $miscName }}</span>
                                                                            </div>
                                                                        </fieldset>
                                                                    </li>
                                                                @endforeach
                                                            </ul>
                                                        </td>
                                                        @else
                                                            <td colspan="5">
                                                                <ul class="list-unstyled mb-0">
                                                                    @foreach ($permission[1] as $misc => $miscName)
                                                                        <li class="d-inline-block mr-2">
                                                                            <fieldset>
                                                                                <div
                                                                                    class="vs-checkbox-con vs-checkbox-primary">
                                                                                    {!! Form::checkbox($misc, '1', $rule->$misc,  ['id' => 'name']) !!}
                                                                                    <span class="vs-checkbox">
                                                                                        <span class="vs-checkbox--check">
                                                                                            <i class="vs-icon feather icon-check"></i>
                                                                                        </span>
                                                                                    </span>
                                                                                    <span>{{ $miscName }}</span>
                                                                                </div>
                                                                            </fieldset>
                                                                        </li>
                                                                    @endforeach
                                                                </ul>
                                                            </td>
                                                        @endif
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 text-center">
                            {!! Form::button('Submit', ['type' => 'submit', 'class' => 'btn btn-primary mr-1 mb-1 waves-effect waves-light']) !!}
                            {!! Form::button('Reset', ['type' => 'reset', 'class' => 'btn btn-outline-warning mr-1 mb-1 waves-effect waves-light']) !!}
                            {!! Form::button('Cancel', ['id' => 'cancel', 'class' => 'btn btn-warning mr-1 mb-1 waves-effect waves-light']) !!}
                        </div>
                    </div>
                </div>

                {{ Form::close() }}
            </div>
        </div>
    </div>
@endsection

@section('script')

@stop
