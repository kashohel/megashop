@extends('Master.Panel')

@section('body')
{{--    @php($topic = ['title' => 'Staff rule', 'breadcrumb' => ['Dashboard' => route('panel-dashboard'), 'Setting' => route('staff-rule.index'), 'staff-rule' => null], 'menu' => ['Add new' => 'addnew', 'Edit' => '', 'Delete' => '']])--}}
{{--    @include('Elements.TopicHeader', ['data' => $topic])--}}

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="mb-0">Staff Rules</h4>
                    <div class="content-header-right text-md-right col-md-3 col-12 d-md-block d-none">
                            <div class="dropdown">
                                <button class="btn-icon btn btn-outline-primary btn-round btn-sm dropdown-toggle waves-effect waves-light" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="feather icon-settings"></i>
                                </button>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <a class="dropdown-item" id="create"><i class="feather icon-plus"></i>Add Staff rule</a>
                                    <a class="dropdown-item" href="#"><i class="feather icon-download"></i>Export as Excel</a>
                                    <a class="dropdown-item" href="#"><i class="feather icon-trash-2"></i>Delete</a>
                                </div>
                            </div>
                    </div>
                </div>
                <div class="card-content listView">
                    <div class="table-responsive mt-1">
                        <table class="table table-hover-animation mb-0">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>RULE NAME</th>
                                <th>ASSIGNED STAFF</th>
                                <th>ACTIVE ASSIGNED</th>
                                <th>LOGIN ACCESS</th>
                                <th class="form-actions right">ACTION</th>
                            </tr>
                            </thead>
                            <tbody id="staff_rule_table">
                            @php($x = 1)
                            @foreach ($rules as $rule)
                                <tr>
                                    <td>{{ $x++ }}</td>
                                    <td>
                                        @if ($rule->assigned->count())
                                            <i class="fa fa-circle font-small-3 text-success mr-50"></i>
                                        @else
                                            <i class="fa fa-circle font-small-3 text-danger mr-50"></i>
                                        @endif
                                        {{ $rule->name }}
                                    </td>
                                    <td>{{ $rule->assigned->count() }}</td>
                                    <td>{{ $rule->active->count() }}</td>
                                    <td>{{ $rule->access->count() }}</td>
                                    <td class="form-actions right">
                                        <a href="{{ route('staff-rule.assigned', $rule->code) }}" class="btn btn-sm btn-icon rounded-circle btn-outline-dark waves-effect waves-light" data-toggle="tooltip" data-placement="top" data-original-title="Assigned Staff">
                                            <i class="ficon feather icon-users"></i></a>
                                        <a href="{{ route('staff-rule.show', $rule->code) }}" class="btn btn-sm btn-icon rounded-circle btn-outline-primary waves-effect waves-light" data-toggle="tooltip" data-placement="top" title="VIew" data-original-title="Todo">
                                            <i class="ficon feather icon-cast"></i></a>
                                        <a href="{{ route('staff-rule.edit', $rule->code) }}"  class="btn btn-sm btn-icon rounded-circle btn-outline-warning waves-effect waves-light" data-value="{{ route('api.warehouse.info', $rule->code) }}" data-toggle="tooltip" data-placement="top" title="Edit" data-original-title="Chat">
                                            <i class="ficon feather icon-message-square"></i></a>
                                        {!! Form::open(['route' => ['staff-rule.destroy', $rule->code], 'method' => 'delete', 'style' => 'display:inline']) !!}
                                        {{ Form::button('<i class="ficon feather icon-calendar"></i></a>', ['type' => 'submit', 'class' => 'btn btn-sm btn-icon rounded-circle btn-outline-danger waves-effect waves-light', 'data-toggle' => "tooltip", 'data-placement' => "top", 'data-original-title' => "Delete"]) }}
                                        {!! Form::close() !!}

                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="card-content create" style="display: none">
                    <div class="card-body">
                        {{ Form::open(['route' => 'staff-rule.store', 'method' => 'post', 'class' => 'form form-horizontal validate']) }}
                            <div class="form-body">
                                <div class="row">
                                    <div class="col-md-4 col-12">
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <span>{{ Form::label('name', 'Rule Name') }}</span>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="form-group position-relative has-icon-left input-divider-left">
                                                {{ Form::text('name', null, ['id' => 'name', 'class' => 'form-control round', 'placeholder' => 'Warehouse Name', 'autofocus', 'required']) }}
                                                <div class="form-control-position">
                                                    <i class="feather icon-grid"></i>
                                                </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-8 col-12">
                                        <div class="form-group row">
                                            <div class="col-md-1">
                                                <span>{{ Form::label('details', 'Details') }}</span>
                                            </div>
                                            <div class="col-md-10">
                                                <div class="form-group position-relative has-icon-left input-divider-left">
                                                {{ Form::text('details', null, ['id' => 'details', 'rows' => 1, 'class' => 'form-control round', 'placeholder' => 'Phone Number', 'required']) }}
                                                <div class="form-control-position">
                                                    <i class="feather icon-file-text"></i>
                                                </div>
                                            </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-12 text-center">
                                        {!! Form::button('Submit', ['type' => 'submit', 'class' => 'btn btn-primary mr-1 mb-1 waves-effect waves-light']) !!}
                                        {!! Form::button('Reset', ['type' => 'reset', 'class' => 'btn btn-outline-warning mr-1 mb-1 waves-effect waves-light']) !!}
                                        {!! Form::button('Cancel', ['id' => 'cancel', 'class' => 'btn btn-warning mr-1 mb-1 waves-effect waves-light']) !!}
                                    </div>
                                </div>
                            </div>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script type="text/javascript">
        $(document).ready(function () {
            $("#create").click(function(){
                $(".create").show("slow");
                $(".listView").hide("slow");
                $(".card-header h4").html("Create new staff rule")
            });
            $("#cancel").click(function(){
                $(".create").hide("slow");
                $(".listView").show("slow");
                $(".card-header h4").html("Staff rule")
            });
        });
    </script>
@stop
