@extends('Master.Panel')

@section('style')
    <style type="text/css">
        .form-group {
            margin-bottom: 9px;
        }
    </style>
    <link rel="stylesheet" type="text/css" href="{{ asset('/Default/app-assets/css/plugins/forms/validation/form-validation.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/Default/app-assets/vendors/css/pickers/pickadate/pickadate.css') }}">
    <link rel="stylesheet" type="text/css" href="../../../app-assets/vendors/css/pickers/pickadate/pickadate.css">
@stop
@section('body')
{{--    @php($topic = ['title' => 'Staff rule', 'breadcrumb' => ['Dashboard' => route('panel-dashboard'), 'Setting' => route('staff-rule.index'), 'staff-rule' => null], 'menu' => ['Add new' => 'addnew', 'Edit' => '', 'Delete' => '']])--}}
{{--    @include('Elements.TopicHeader', ['data' => $topic])--}}

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="mb-0">Staff List</h4>
                    <div class="content-header-right text-md-right col-md-3 col-12 d-md-block d-none">
                            <div class="dropdown">
                                <button class="btn-icon btn btn-outline-primary btn-round btn-sm dropdown-toggle waves-effect waves-light" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="feather icon-settings"></i>
                                </button>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <a href="{{ route('staff-rule.index') }}" class="dropdown-item"><i class="feather icon-plus"></i>Staff rule list</a>
                                    <a class="dropdown-item" id="create"><i class="feather icon-plus"></i>Add New Staff</a>
                                    <a class="dropdown-item" href="#"><i class="feather icon-trash-2"></i>Delete</a>
                                </div>
                            </div>
                    </div>
                </div>
                <div class="card-content listView">
                    <div class="table-responsive mt-1">
                        <table class="table table-hover-animation mb-0">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>FULL NAME</th>
                                <th>PHONE</th>
                                <th>RULE</th>
                                <th>JOINING DATE</th>
                                <th>LOGIN ACCESS</th>
                                <th>STATUS</th>
                                <th class="form-actions right">ACTION</th>
                            </tr>
                            </thead>
                            <tbody id="staff_rule_table">
                            @php($x = 1)
                            @foreach ($staffs as $staff)
                                <tr>
                                    <td>{{ $x++ }}</td>
                                    <td>{{ $staff->first_name." ".$staff->last_name }}</td>
                                    <td>{{ $staff->mobile }}</td>
                                    <td>{{ $staff->rule->name??"Super User" }}</td>
                                    <td>{{ \Carbon\Carbon::parse($staff->joining_date)->format('d-M-Y') }}</td>
                                    <td>
                                        @if ($staff->username && $staff->password)
                                            {{ "Accessible" }}
                                        @else
                                            {{ "No Access" }}
                                        @endif
                                    </td>
                                    <td>
                                        @if ($staff->active)
                                            <a href="{{ route('staff.changeStatus', $staff->code) }}"  class="btn btn-sm btn-icon rounded-circle btn-outline-success waves-effect waves-light change-status-active" data-toggle="tooltip" data-placement="top" title="Activated" data-original-title="Chat">
                                                <i class="ficon feather icon-check-circle"></i></a>
                                        @else
                                            <a href="{{ route('staff.changeStatus', $staff->code) }}"  class="btn btn-sm btn-icon rounded-circle btn-outline-danger waves-effect waves-light change-status-inactive" data-toggle="tooltip" data-placement="top" title="Deactivated" data-original-title="Chat">
                                                <i class="ficon feather icon-x-circle"></i></a>
                                        @endif
                                    </td>

{{--    TODO need to change button link as staff list page button link--}}

                                    <td class="form-actions right">
{{--                                        <a href="{{ route('staff.assigned', $staff->code) }}" class="btn btn-sm btn-icon rounded-circle btn-outline-dark waves-effect waves-light" data-toggle="tooltip" data-placement="top" data-original-title="Staff List">--}}
{{--                                            <i class="ficon feather icon-users"></i></a>--}}
                                        <a href="{{ route('staff.show', $staff->code) }}" class="btn btn-sm btn-icon rounded-circle btn-outline-primary waves-effect waves-light" data-toggle="tooltip" data-placement="top" title="Profile" data-original-title="Profile">
                                            <i class="ficon feather icon-clipboard"></i></a>
                                        <a href="{{ route('staff.edit', $staff->code) }}"  class="btn btn-sm btn-icon rounded-circle btn-outline-warning waves-effect waves-light" data-value="{{ route('api.warehouse.info', $staff->code) }}" data-toggle="tooltip" data-placement="top" title="Edit" data-original-title="Chat">
                                            <i class="ficon feather icon-edit"></i></a>
                                        {!! Form::open(['route' => ['staff.destroy', $staff->code], 'method' => 'delete', 'style' => 'display:inline']) !!}
                                        {{ Form::button('<i class="ficon feather icon-calendar"></i></a>', ['type' => 'submit', 'class' => 'btn btn-sm btn-icon rounded-circle btn-outline-danger waves-effect waves-light', 'data-toggle' => "tooltip", 'data-placement' => "top", 'data-original-title' => "Delete"]) }}
                                        {!! Form::close() !!}

                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="card-content create" style="display: none">
                    <div class="card-body">
                        {{ Form::open(['route' => 'staff.store', 'method' => 'post', 'class' => 'form form-horizontal validate', 'files' => true, 'novalidate']) }}
                        <div class="form-body">
                            <div class="divider">
                                <div class="divider-text">Personal Information</div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 col-12">
                                    <div class="form-group row">
                                        <div class="col-md-4">
                                            <span>{{ Form::label('first_name', 'First Name') }}</span>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="form-group position-relative has-icon-left input-divider-left">
                                                {{ Form::text('first_name', null, ['id' => 'first_name', 'class' => 'form-control round', 'placeholder' => 'First Part of Name', 'autofocus', 'required']) }}
                                                <div class="form-control-position">
                                                    <i class="feather icon-user"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="form-group row">
                                        <div class="col-md-4">
                                            <span>{{ Form::label('last_name', 'Last Name') }}</span>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="form-group position-relative has-icon-left input-divider-left">
                                                {{ Form::text('last_name', null, ['id' => 'last_name', 'class' => 'form-control round', 'placeholder' => 'Last Part of Name', 'required']) }}
                                                <div class="form-control-position">
                                                    <i class="feather icon-user"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="form-group row">
                                        <div class="col-md-4">
                                            <span>{{ Form::label('mobile', 'Mobile Number') }}</span>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="form-group position-relative has-icon-left input-divider-left">
                                                {{ Form::tel('mobile', null, ['id' => 'mobile', 'class' => 'form-control round', 'placeholder' => 'Active Mobile Number', 'required']) }}
                                                <div class="form-control-position">
                                                    <i class="feather icon-phone"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="form-group row">
                                        <div class="col-md-4">
                                            <span>{{ Form::label('phone', 'Phone Number') }}</span>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="form-group position-relative has-icon-left input-divider-left">
                                                {{ Form::text('phone', null, ['id' => 'phone', 'class' => 'form-control round', 'placeholder' => 'Another Phone Number']) }}
                                                <div class="form-control-position">
                                                    <i class="feather icon-phone"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="form-group row">
                                        <div class="col-md-4">
                                            <span>{{ Form::label('email', 'Email Address') }}</span>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="form-group position-relative has-icon-left input-divider-left">
                                                {{ Form::text('email', null, ['id' => 'email', 'class' => 'form-control round', 'placeholder' => 'Email Address']) }}
                                                <div class="form-control-position">
                                                    <i class="feather icon-mail"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="form-group row">
                                        <div class="col-md-4">
                                            <span>{{ Form::label('nid', 'NID Number') }}</span>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="form-group position-relative has-icon-left input-divider-left">
                                                {{ Form::number('nid', null, ['id' => 'nid', 'class' => 'form-control round', 'placeholder' => 'National Identification Number']) }}
                                                <div class="form-control-position">
                                                    <i class="feather icon-clipboard"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="form-group row">
                                        <div class="col-md-4">
                                            <span>{{ Form::label('gender', 'Gender') }}</span>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="form-group position-relative has-icon-left input-divider-left">
                                                {{ Form::select('gender', ["Female", "Male", "Other"], null, ['id' => 'gender', 'class' => 'form-control round', 'placeholder' => 'Select Gender', 'required']) }}
                                                <div class="form-control-position">
                                                    <i class="feather icon-anchor"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="form-group row">
                                        <div class="col-md-4">
                                            <span>{{ Form::label('marital_status', 'Marital Status') }}</span>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="form-group position-relative has-icon-left input-divider-left">
                                                {{ Form::select('marital_status', ['single' => 'Single', 'married' => 'Married', 'widowed' => 'Widowed', 'divorced' => 'Divorced'], null, ['id' => 'marital_status', 'class' => 'form-control round', 'placeholder' => 'Select Marital status']) }}
                                                <div class="form-control-position">
                                                    <i class="feather icon-user-plus"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="form-group row">
                                        <div class="col-md-4">
                                            <span>{{ Form::label('birthday', 'Birthday') }}</span>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="form-group position-relative has-icon-left input-divider-left">
                                                {{ Form::date('birthday', null, ['id' => 'birthday', 'class' => 'form-control pickadate-months-year round', 'placeholder' => 'Click to select Birthday']) }}
                                                <div class="form-control-position">
                                                    <i class="feather icon-calendar"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="form-group row">
                                        <div class="col-md-4">
                                            <span>{{ Form::label('address', 'Permanent Address') }}</span>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="form-group position-relative has-icon-left input-divider-left">
                                                {{ Form::textarea('address', $staff->address, ['id' => 'address', 'rows' => 1, 'class' => 'form-control round', 'placeholder' => 'Permanent location address']) }}
                                                <div class="form-control-position">
                                                    <i class="feather icon-map-pin"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="divider">
                                <div class="divider-text">Office Information</div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 col-12">
                                    <div class="form-group row">
                                        <div class="col-md-4">
                                            <span>{{ Form::label('designation', 'Designation') }}</span>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="form-group position-relative has-icon-left input-divider-left">
                                                {{ Form::text('designation', null, ['id' => 'designation', 'class' => 'form-control round', 'placeholder' => 'Input designation', 'required']) }}
                                                <div class="form-control-position">
                                                    <i class="feather icon-pocket"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="form-group row">
                                        <div class="col-md-4">
                                            <span>{{ Form::label('price_group', 'Price Group') }}</span>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="form-group position-relative has-icon-left input-divider-left">
                                                {{ Form::select('price_group', $prices->pluck('name', 'code'), null, ['id' => 'price_group', 'class' => 'form-control round', 'placeholder' => 'Default Price Group']) }}
                                                <div class="form-control-position">
                                                    <i class="feather icon-grid"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="form-group row">
                                        <div class="col-md-4">
                                            <span>{{ Form::label('joining', 'Joining Date') }}</span>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="form-group position-relative has-icon-left input-divider-left">
                                                {{ Form::date('joining', null, ['id' => 'joining', 'class' => 'form-control pickadate round', 'placeholder' => 'Today | Select another']) }}
                                                <div class="form-control-position">
                                                    <i class="feather icon-calendar"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="form-group row">
                                        <div class="col-md-4">
                                            <span>{{ Form::label('warehouse', 'Warehouse') }}</span>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="form-group position-relative has-icon-left input-divider-left">
                                                {{ Form::select('warehouse', $warehouses->pluck('name', 'code'), null, ['id' => 'warehouse', 'class' => 'form-control round', 'placeholder' => 'Select Warehouse']) }}
                                                <div class="form-control-position">
                                                    <i class="feather icon-pie-chart"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="form-group row">
                                        <div class="col-md-4">
                                            <span>{{ Form::label('details', 'Short Note') }}</span>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="form-group position-relative has-icon-left input-divider-left">
                                                {{ Form::textarea('details', null, ['id' => 'details', 'rows' => 1, 'class' => 'form-control round', 'placeholder' => 'Details about staff']) }}
                                                <div class="form-control-position">
                                                    <i class="feather icon-server"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="form-group row">
                                        <div class="col-md-4">
                                            <span>{{ Form::label('rule', 'Rule Select') }}</span>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="form-group position-relative has-icon-left input-divider-left">
                                                {{ Form::select('rule', $rules->pluck('name', 'code'), null, ['id' => 'rule', 'class' => 'form-control round', 'placeholder' => 'Super User']) }}
                                                <div class="form-control-position">
                                                    <i class="feather icon-layers"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="divider">
                                <div class="divider-text">Additional Information</div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 col-12">
                                    <div class="form-group row">
                                        <div class="col-md-4">
                                            <span>{{ Form::label('profile', 'Profile Picture') }}</span>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="form-group position-relative has-icon-left input-divider-left">
                                                {{ Form::file('profile', ['id' => 'profile', 'class' => 'form-control round', 'placeholder' => 'Profile Photo',  'accept' => ".jpeg, .png, .svg, .bmp, .gif"]) }}
                                                <div class="form-control-position">
                                                    <i class="feather icon-image"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="form-group row">
                                        <div class="col-md-4">
                                            <span>{{ Form::label('cv', 'Resume File') }}</span>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="form-group position-relative has-icon-left input-divider-left">
                                                {{ Form::file('cv', ['id' => 'cv', 'class' => 'form-control round', 'placeholder' => 'Curriculum Vitae', 'accept' => '.doc,.docx,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document']) }}
                                                <div class="form-control-position">
                                                    <i class="feather icon-file-plus"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="divider">
                                <div class="divider-text"></div>
                            </div>
                            <div class="row">
                                <div class="col-12 text-center">
                                    {!! Form::button('Submit', ['type' => 'submit', 'class' => 'btn btn-primary mr-1 mb-1 waves-effect waves-light']) !!}
                                    {!! Form::button('Reset', ['type' => 'reset', 'class' => 'btn btn-outline-warning mr-1 mb-1 waves-effect waves-light']) !!}
                                    {!! Form::button('Cancel', ['id' => 'cancel', 'class' => 'btn btn-warning mr-1 mb-1 waves-effect waves-light']) !!}
                                </div>
                            </div>
                        </div>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script type="text/javascript">
        $(document).ready(function () {
            $("#create").click(function(){
                $(".create").show("slow");
                $(".listView").hide("slow");
                $(".card-header h4").html("Create new staff")
            });
            $("#cancel").click(function(){
                $(".create").hide("slow");
                $(".listView").show("slow");
                $(".card-header h4").html("Staff List")
            });
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $(".change-status-active").mouseover(function () {
                $(this).children("i").removeClass('icon-check-circle').addClass('icon-refresh-cw');
            }).mouseout(function () {
                $(this).children("i").addClass('icon-check-circle').removeClass('icon-refresh-cw');
            });
            $(".change-status-inactive").mouseover(function () {
                $(this).children("i").addClass('icon-refresh-cw').removeClass('icon-x-circle');
            }).mouseout(function () {
                $(this).children("i").addClass('icon-x-circle').removeClass('icon-refresh-cw');
            })
        })
    </script>

    <script src="{{ asset('Default/app-assets/vendors/js/forms/validation/jqBootstrapValidation.js') }}"></script>
    <script src="{{ asset('Default/app-assets/js/scripts/forms/validation/form-validation.js') }}"></script>
    <script src="{{ asset('Default/app-assets/vendors/js/pickers/pickadate/picker.js') }}"></script>
    <script src="{{ asset('Default/app-assets/vendors/js/pickers/pickadate/picker.date.js') }}"></script>
    <script src="{{ asset('Default/app-assets/js/scripts/pickers/dateTime/pick-a-datetime.js') }}"></script>
@stop
