@extends('Master.Panel')

@section('body')
{{--    @php($topic = ['title' => 'Warehouse', 'breadcrumb' => ['Dashboard' => route('panel-dashboard'), 'Setting' => route('warehouse.index'), 'Warehouse' => null], 'menu' => ['Add new' => 'addnew', 'Edit' => '', 'Delete' => '']])--}}
{{--    @include('Elements.TopicHeader', ['data' => $topic])--}}

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="mb-0">Warehouse List</h4>
                    <div class="content-header-right text-md-right col-md-3 col-12 d-md-block d-none">
                            <div class="dropdown">
                                <button class="btn-icon btn btn-outline-primary btn-round btn-sm dropdown-toggle waves-effect waves-light" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="feather icon-settings"></i>
                                </button>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <a class="dropdown-item" id="create"><i class="feather icon-plus"></i>Add Warehouse</a>
                                    <a class="dropdown-item" href="#"><i class="feather icon-download"></i>Export as Excel</a>
                                    <a class="dropdown-item" href="#"><i class="feather icon-trash-2"></i>Delete</a>
                                </div>
                            </div>
                    </div>
                </div>
                <div class="card-content listView">
                    <div class="table-responsive mt-1">
                        <table class="table table-hover-animation mb-0">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>NAME</th>
                                <th>PHONE</th>
                                <th>EMAIL</th>
                                <th>PRICE GROUP</th>
                                <th class="form-actions right">ACTION</th>
                            </tr>
                            </thead>
                            <tbody id="warehouse_table">
                            @php($x = 1)
                            @foreach ($warehouses as $warehouse)
                                <tr>
                                    <td>{{ $x++ }}</td>
                                    <td>
                                        <i class="fa fa-circle font-small-3 text-success mr-50"></i>{{ $warehouse->name }}
                                    </td>
                                    <td>{{ $warehouse->phone }}</td>
                                    <td>{{ $warehouse->email }}</td>
                                    <td>{{ $warehouse->price_group??"Default group" }}</td>
                                    <td class="form-actions right">
                                        <a href="#" data-toggle="tooltip" data-placement="top" title="VIew" data-original-title="Todo">
                                            <i class="ficon feather icon-check-square"></i></a>
                                        <a class="editForm" data-value="{{ route('api.warehouse.info', $warehouse->code) }}" data-toggle="tooltip" data-placement="top" title="Edit" data-original-title="Chat">
                                            <i class="ficon feather icon-message-square"></i></a>
                                        <a href="#" data-toggle="tooltip" data-placement="top" title="" data-original-title="Email">
                                            <i class="ficon feather icon-mail"></i></a>
                                        <a href="#" data-toggle="tooltip" data-placement="top" title="" data-original-title="Calendar">
                                            <i class="ficon feather icon-calendar"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="card-content create" style="display: none">
                    <div class="card-body">
                        {{ Form::open(['route' => 'warehouse.store', 'method' => 'post', 'class' => 'form form-horizontal validate']) }}
                            <div class="form-body">
                                <div class="row">
                                    <div class="col-md-6 col-12">
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <span>{{ Form::label('name', 'Warehouse Name') }}</span>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="form-group position-relative has-icon-left input-divider-left">
                                                {{ Form::text('name', null, ['id' => 'name', 'class' => 'form-control round', 'placeholder' => 'Warehouse Name', 'autofocus', 'required']) }}
                                                <div class="form-control-position">
                                                    <i class="feather icon-grid"></i>
                                                </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <span>{{ Form::label('phone', 'Phone Number') }}</span>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="form-group position-relative has-icon-left input-divider-left">
                                                {{ Form::tel('phone', null, ['id' => 'phone', 'class' => 'form-control round', 'placeholder' => 'Phone Number', 'required']) }}
                                                <div class="form-control-position">
                                                    <i class="feather icon-phone-call"></i>
                                                </div>
                                            </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <span>{{ Form::label('price_group') }}</span>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="form-group position-relative has-icon-left input-divider-left">
                                                {{ Form::select('price_group', $priceGroup->pluck('name', 'code'), null, ['id' => 'price_group', 'class' => 'form-control round', 'placeholder' => 'Default Price Group']) }}
                                                <div class="form-control-position">
                                                    <i class="feather icon-credit-card"></i>
                                                </div>
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <span>{{ Form::label('email', 'Email Address') }}</span>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="form-group position-relative has-icon-left input-divider-left">
                                                {{ Form::email('email', null, ['id' => 'email', 'class' => 'form-control round', 'placeholder' => 'Email Address']) }}
                                                <div class="form-control-position">
                                                    <i class="feather icon-mail"></i>
                                                </div>
                                            </div>
                                        </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6 col-12">
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <span>{{ Form::label('map_point') }}</span>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="form-group position-relative has-icon-left input-divider-left">
                                                {{ Form::text('map_point', null, ['id' => 'price_group', 'class' => 'form-control round', 'placeholder' => 'Input ligature, latitude']) }}
                                                <div class="form-control-position">
                                                    <i class="feather icon-map-pin"></i>
                                                </div>
                                            </div>
                                        </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6 col-12">
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <span>{{ Form::label('address') }}</span>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="form-group position-relative has-icon-left input-divider-left">
                                                {{ Form::text('address', null, ['id' => 'price_group', 'class' => 'form-control round', 'placeholder' => 'Input full address']) }}
                                                <div class="form-control-position">
                                                    <i class="feather icon-map"></i>
                                                </div>
                                            </div>
                                        </div>
                                        </div>
                                    </div>

                                    <div class="col-12 text-center">
                                        {!! Form::button('Submit', ['type' => 'submit', 'class' => 'btn btn-primary mr-1 mb-1 waves-effect waves-light']) !!}
                                        {!! Form::button('Reset', ['type' => 'reset', 'class' => 'btn btn-outline-warning mr-1 mb-1 waves-effect waves-light']) !!}
                                        {!! Form::button('Cancel', ['id' => 'cancel', 'class' => 'btn btn-warning mr-1 mb-1 waves-effect waves-light']) !!}
                                    </div>
                                </div>
                            </div>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script type="text/javascript">
        $(document).ready(function () {
            $("#create").click(function(){
                $(".create").show("slow");
                $(".listView").hide("slow");
                $(".card-header h4").html("Add new Warehouse")
            });
            $("#cancel").click(function(){
                $(".create").hide("slow");
                $(".listView").show("slow");
                $(".card-header h4").html("Warehouse List")
            });
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $.ajax({
                type: 'get',
                url: "{{ URL::route('api.warehouse.list') }}",
                data: {
                    '_token': $('input[name=_token]').val(),
                    'branch': $("#branch").val(),
                    'bank': $("#account_id").val()
                },
                success: function (data) {
                    var warehouse_data = "";
                    var serial = 1;
                    $.each(data, function (key, value) {
                        alert(key);
                        warehouse_data += '<tr>';
                        // warehouse_data += '<td>'+value.parseInt(serial+1)+'</tr>';
                        warehouse_data += '<td>'+value.name+'</tr>';
                        warehouse_data += '<td>'+value.phone+'</tr>';
                        warehouse_data += '<td>'+value.email+'</tr>';
                        warehouse_data += '<td>'+value.email+'</tr>';
                        warehouse_data += '<td>'+value.email+'</tr>';
                        warehouse_data += '<td>'+value.email+'</tr>';
                        warehouse_data += '</tr>';
                    });
                    alert(warehouse_data);
                    $("#warehouse_table").append(warehouse_data);
                }
            });
        })
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $(".editForm").click(function(){
                $.ajax({
                    type: 'get',
                    url: $(this).data('value'),
                    success: function (data) {
                        alert(data);
                    }
                });
            });
        })
    </script>
@stop
