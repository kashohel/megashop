@extends('Master.Panel')

@section('style')
    <style type="text/css">
        .form-group {
            margin-bottom: 9px;
        }
    </style>
    <link rel="stylesheet" type="text/css" href="{{ asset('/Default/app-assets/css/plugins/forms/validation/form-validation.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/Default/app-assets/vendors/css/pickers/pickadate/pickadate.css') }}">
    <link rel="stylesheet" type="text/css" href="../../../app-assets/vendors/css/pickers/pickadate/pickadate.css">
@stop
@section('body')
{{--    @php($topic = ['title' => 'Staff rule', 'breadcrumb' => ['Dashboard' => route('panel-dashboard'), 'Setting' => route('staff-rule.index'), 'staff-rule' => null], 'menu' => ['Add new' => 'addnew', 'Edit' => '', 'Delete' => '']])--}}
{{--    @include('Elements.TopicHeader', ['data' => $topic])--}}

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="mb-0">Update Staff information</h4>
                    <div class="content-header-right text-md-right col-md-3 col-12 d-md-block d-none">
                            <div class="dropdown">
                                <button class="btn-icon btn btn-outline-primary btn-round btn-sm dropdown-toggle waves-effect waves-light" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="feather icon-settings"></i>
                                </button>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <a href="{{ route('staff-rule.index') }}" class="dropdown-item"><i class="feather icon-plus"></i>Staff rule list</a>
                                    <a class="dropdown-item" id="create"><i class="feather icon-plus"></i>Add New Staff</a>
                                    <a class="dropdown-item" href="#"><i class="feather icon-trash-2"></i>Delete</a>
                                </div>
                            </div>
                    </div>
                </div>
                <div class="card-content create">
                    <div class="card-body">
                        {{ Form::open(['route' => ['staff.update', $staff->code], 'method' => 'put', 'class' => 'form form-horizontal validate', 'files' => true, 'novalidate']) }}
                            <div class="form-body">
                                <div class="divider">
                                    <div class="divider-text">Personal Information</div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 col-12">
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <span>{{ Form::label('first_name', 'First Name') }}</span>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="form-group position-relative has-icon-left input-divider-left">
                                                {{ Form::text('first_name', $staff->first_name, ['id' => 'first_name', 'class' => 'form-control round', 'placeholder' => 'First Part of Name', 'autofocus', 'required']) }}
                                                <div class="form-control-position">
                                                    <i class="feather icon-user"></i>
                                                </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <span>{{ Form::label('last_name', 'Last Name') }}</span>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="form-group position-relative has-icon-left input-divider-left">
                                                {{ Form::text('last_name', $staff->last_name, ['id' => 'last_name', 'class' => 'form-control round', 'placeholder' => 'Last Part of Name', 'required']) }}
                                                <div class="form-control-position">
                                                    <i class="feather icon-user"></i>
                                                </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <span>{{ Form::label('mobile', 'Mobile Number') }}</span>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="form-group position-relative has-icon-left input-divider-left">
                                                {{ Form::tel('mobile', $staff->mobile, ['id' => 'mobile', 'class' => 'form-control round', 'placeholder' => 'Active Mobile Number', 'required']) }}
                                                <div class="form-control-position">
                                                    <i class="feather icon-phone"></i>
                                                </div>
                                            </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <span>{{ Form::label('phone', 'Phone Number') }}</span>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="form-group position-relative has-icon-left input-divider-left">
                                                {{ Form::text('phone', $staff->phone, ['id' => 'phone', 'class' => 'form-control round', 'placeholder' => 'Another Phone Number']) }}
                                                <div class="form-control-position">
                                                    <i class="feather icon-phone"></i>
                                                </div>
                                            </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <span>{{ Form::label('email', 'Email Address') }}</span>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="form-group position-relative has-icon-left input-divider-left">
                                                    {{ Form::text('email', $staff->email, ['id' => 'email', 'class' => 'form-control round', 'placeholder' => 'Email Address']) }}
                                                    <div class="form-control-position">
                                                        <i class="feather icon-mail"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <span>{{ Form::label('nid', 'NID Number') }}</span>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="form-group position-relative has-icon-left input-divider-left">
                                                {{ Form::number('nid', $staff->nid, ['id' => 'nid', 'class' => 'form-control round', 'placeholder' => 'National Identification Number']) }}
                                                <div class="form-control-position">
                                                    <i class="feather icon-clipboard"></i>
                                                </div>
                                            </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <span>{{ Form::label('gender', 'Gender') }}</span>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="form-group position-relative has-icon-left input-divider-left">
                                                {{ Form::select('gender', ["Female", "Male", "Other"], $staff->gender, ['id' => 'gender', 'class' => 'form-control round', 'placeholder' => 'Select Gender', 'required']) }}
                                                <div class="form-control-position">
                                                    <i class="feather icon-anchor"></i>
                                                </div>
                                            </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <span>{{ Form::label('marital_status', 'Marital Status') }}</span>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="form-group position-relative has-icon-left input-divider-left">
                                                {{ Form::select('marital_status', ['single' => 'Single', 'married' => 'Married', 'widowed' => 'Widowed', 'divorced' => 'Divorced'], $staff->marital_status, ['id' => 'marital_status', 'class' => 'form-control round', 'placeholder' => 'Select Marital status']) }}
                                                <div class="form-control-position">
                                                    <i class="feather icon-user-plus"></i>
                                                </div>
                                            </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <span>{{ Form::label('birthday', 'Birthday') }}</span>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="form-group position-relative has-icon-left input-divider-left">
                                                {{ Form::date('birthday', $staff->birthday?\Carbon\Carbon::parse($staff->birthday)->format('j F, Y'):null, ['id' => 'birthday', 'class' => 'form-control pickadate-months-year round', 'placeholder' => 'Click to select Birthday']) }}
                                                <div class="form-control-position">
                                                    <i class="feather icon-calendar"></i>
                                                </div>
                                            </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <span>{{ Form::label('address', 'Permanent Address') }}</span>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="form-group position-relative has-icon-left input-divider-left">
                                                    {{ Form::textarea('address', $staff->address, ['id' => 'address', 'rows' => 1, 'class' => 'form-control round', 'placeholder' => 'Permanent location address']) }}
                                                    <div class="form-control-position">
                                                        <i class="feather icon-map-pin"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="divider">
                                    <div class="divider-text">Office Information</div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 col-12">
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <span>{{ Form::label('designation', 'Designation') }}</span>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="form-group position-relative has-icon-left input-divider-left">
                                                {{ Form::text('designation', $staff->designation, ['id' => 'designation', 'class' => 'form-control round', 'placeholder' => 'Input designation', 'required']) }}
                                                <div class="form-control-position">
                                                    <i class="feather icon-pocket"></i>
                                                </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <span>{{ Form::label('price_group', 'Price Group') }}</span>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="form-group position-relative has-icon-left input-divider-left">
                                                {{ Form::select('price_group', $prices->pluck('name', 'code'), $staff->priceGroup?$staff->priceGroup->code:null, ['id' => 'price_group', 'class' => 'form-control round', 'placeholder' => 'Default Price Group']) }}
                                                <div class="form-control-position">
                                                    <i class="feather icon-grid"></i>
                                                </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <span>{{ Form::label('joining', 'Joining Date') }}</span>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="form-group position-relative has-icon-left input-divider-left">
                                                {{ Form::date('joining', \Carbon\Carbon::parse($staff->joining_date)->format('d F, Y'), ['id' => 'joining', 'class' => 'form-control pickadate round', 'placeholder' => 'Today | Select another']) }}
                                                <div class="form-control-position">
                                                    <i class="feather icon-calendar"></i>
                                                </div>
                                            </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <span>{{ Form::label('warehouse', 'Warehouse') }}</span>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="form-group position-relative has-icon-left input-divider-left">
                                                {{ Form::select('warehouse', $warehouses->pluck('name', 'code'), $staff->warehouse?$staff->warehouse->code:null, ['id' => 'warehouse', 'class' => 'form-control round', 'placeholder' => 'Select Warehouse']) }}
                                                <div class="form-control-position">
                                                    <i class="feather icon-pie-chart"></i>
                                                </div>
                                            </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <span>{{ Form::label('details', 'Short Note') }}</span>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="form-group position-relative has-icon-left input-divider-left">
                                                    {{ Form::textarea('details', $staff->details, ['id' => 'details', 'rows' => 1, 'class' => 'form-control round', 'placeholder' => 'Details about staff']) }}
                                                    <div class="form-control-position">
                                                        <i class="feather icon-server"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <span>{{ Form::label('rule', 'Rule Select') }}</span>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="form-group position-relative has-icon-left input-divider-left">
                                                {{ Form::select('rule', $rules->pluck('name', 'code'), $staff->rule_id?$staff->rule->code:null, ['id' => 'rule', 'class' => 'form-control round', 'placeholder' => 'Super User']) }}
                                                <div class="form-control-position">
                                                    <i class="feather icon-layers"></i>
                                                </div>
                                            </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="divider">
                                    <div class="divider-text">Additional Information</div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 col-12">
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <span>{{ Form::label('profile', 'Profile Picture') }}</span>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="form-group position-relative has-icon-left input-divider-left">
                                                {{ Form::file('profile', ['id' => 'profile', 'class' => 'form-control round', 'placeholder' => 'Profile Photo',  'accept' => ".jpeg, .png, .svg, .bmp, .gif"]) }}
                                                <div class="form-control-position">
                                                    <i class="feather icon-image"></i>
                                                </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <span>{{ Form::label('cv', 'Resume File') }}</span>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="form-group position-relative has-icon-left input-divider-left">
                                                {{ Form::file('cv', ['id' => 'cv', 'class' => 'form-control round', 'placeholder' => 'Curriculum Vitae', 'accept' => '.doc,.docx,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document']) }}
                                                <div class="form-control-position">
                                                    <i class="feather icon-file-plus"></i>
                                                </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="divider">
                                    <div class="divider-text"></div>
                                </div>
                                <div class="row">
                                    <div class="col-12 text-center">
                                        {!! Form::button('Submit', ['type' => 'submit', 'class' => 'btn btn-primary mr-1 mb-1 waves-effect waves-light']) !!}
                                        {!! Form::button('Reset', ['type' => 'reset', 'class' => 'btn btn-outline-warning mr-1 mb-1 waves-effect waves-light']) !!}
                                        {!! Form::button('Cancel', ['id' => 'cancel', 'class' => 'btn btn-warning mr-1 mb-1 waves-effect waves-light']) !!}
                                    </div>
                                </div>
                            </div>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script type="text/javascript">
        $(document).ready(function () {
            $("#create").click(function(){
                $(".create").show("slow");
                $(".listView").hide("slow");
                $(".card-header h4").html("Create new staff")
            });
            $("#cancel").click(function(){
                $(".create").hide("slow");
                $(".listView").show("slow");
                $(".card-header h4").html("Staff List")
            });
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $(".change-status-active").mouseover(function () {
                $(this).children("i").removeClass('icon-check-circle').addClass('icon-refresh-cw');
            }).mouseout(function () {
                $(this).children("i").addClass('icon-check-circle').removeClass('icon-refresh-cw');
            });
            $(".change-status-inactive").mouseover(function () {
                $(this).children("i").addClass('icon-refresh-cw').removeClass('icon-x-circle');
            }).mouseout(function () {
                $(this).children("i").addClass('icon-x-circle').removeClass('icon-refresh-cw');
            })
        })
    </script>

    <script src="{{ asset('Default/app-assets/vendors/js/forms/validation/jqBootstrapValidation.js') }}"></script>
    <script src="{{ asset('Default/app-assets/js/scripts/forms/validation/form-validation.js') }}"></script>
    <script src="{{ asset('Default/app-assets/vendors/js/pickers/pickadate/picker.js') }}"></script>
    <script src="{{ asset('Default/app-assets/vendors/js/pickers/pickadate/picker.date.js') }}"></script>
    <script src="{{ asset('Default/app-assets/js/scripts/pickers/dateTime/pick-a-datetime.js') }}"></script>
@stop
