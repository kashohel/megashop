@extends('Master.Panel')

@section('style')
    <style type="text/css">
        .form-group {
            margin-bottom: 9px;
        }
    </style>
    <link rel="stylesheet" type="text/css" href="{{ asset('/Default/app-assets/css/plugins/forms/validation/form-validation.css') }}">
@stop
@section('body')
{{--    @php($topic = ['title' => 'Staff rule', 'breadcrumb' => ['Dashboard' => route('panel-dashboard'), 'Setting' => route('staff-rule.index'), 'staff-rule' => null], 'menu' => ['Add new' => 'addnew', 'Edit' => '', 'Delete' => '']])--}}
{{--    @include('Elements.TopicHeader', ['data' => $topic])--}}

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="mb-0">Brand List</h4>
                    <div class="content-header-right text-md-right col-md-3 col-12 d-md-block d-none">
                            <div class="dropdown">
                                <button class="btn-icon btn btn-outline-primary btn-round btn-sm dropdown-toggle waves-effect waves-light" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="feather icon-settings"></i>
                                </button>
                                <div class="dropdown-menu dropdown-menu-right">
{{--                                    <a href="{{ route('staff-rule.index') }}" class="dropdown-item"><i class="feather icon-plus"></i>Staff rule list</a>--}}
                                    <a class="dropdown-item" id="create"><i class="feather icon-plus"></i>Add New Brand</a>
                                    <a class="dropdown-item" href="#"><i class="feather icon-trash-2"></i>Delete</a>
                                </div>
                            </div>
                    </div>
                </div>
                <div class="card-content listView">
                    <div class="table-responsive mt-1">
                        <table class="table table-hover-animation mb-0">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>LOGO</th>
                                <th>NAME</th>
                                <th>CODE</th>
                                <th>SLUG</th>
                                <th class="form-actions right">ACTION</th>
                            </tr>
                            </thead>
                            <tbody id="staff_rule_table">
                            @php($x = 1)
                            @foreach ($brands as $brand)
                                <tr>
                                    <td>{{ $x++ }}</td>
                                    <td>
                                        <div class="avatar mr-1 ">
                                            @if ($brand->logo)
                                                <img src="{{ asset('/storage/Brand/'.$brand->logo) }}" alt="avtar img holder" height="32" width="32">
                                            @else
                                                <span class="avatar-content">{{ explode(' ', $brand->name)[0] }}</span>
                                            @endif
                                        </div>
                                    </td>
                                    <td>{{ $brand->name }}</td>
                                    <td>{{ $brand->code }}</td>
                                    <td>{{ $brand->slug }}</td>

{{--    TODO need to change button link as staff list page button link--}}

                                    <td class="form-actions right">
{{--                                        <a href="{{ route('staff.assigned', $brand->code) }}" class="btn btn-sm btn-icon rounded-circle btn-outline-dark waves-effect waves-light" data-toggle="tooltip" data-placement="top" data-original-title="Staff List">--}}
{{--                                            <i class="ficon feather icon-users"></i></a>--}}
                                        <a href="{{ route('brand.show', $brand->code) }}" class="btn btn-sm btn-icon rounded-circle btn-outline-primary waves-effect waves-light" data-toggle="tooltip" data-placement="top" title="Brand Products" data-original-title="Profile">
                                            <i class="ficon feather icon-clipboard"></i></a>
                                        <a href="{{ route('brand.edit', $brand->code) }}"  class="btn btn-sm btn-icon rounded-circle btn-outline-warning waves-effect waves-light" data-value="{{ route('api.warehouse.info', $brand->code) }}" data-toggle="tooltip" data-placement="top" title="Edit" data-original-title="Chat">
                                            <i class="ficon feather icon-edit"></i></a>
                                        {!! Form::open(['route' => ['brand.destroy', $brand->code], 'method' => 'delete', 'style' => 'display:inline']) !!}
                                        {{ Form::button('<i class="ficon feather icon-trash"></i></a>', ['type' => 'submit', 'class' => 'btn btn-sm btn-icon rounded-circle btn-outline-danger waves-effect waves-light', 'data-toggle' => "tooltip", 'data-placement' => "top", 'data-original-title' => "Delete"]) }}
                                        {!! Form::close() !!}

                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="card-content create" style="display: none">
                    <div class="card-body">
                        {{ Form::open(['route' => 'brand.store', 'method' => 'post', 'class' => 'form form-horizontal validate', 'files' => true, 'novalidate']) }}
                        <div class="form-body">
                            <div class="divider">
                                <div class="divider-text">Brand Information</div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 col-12">
                                    <div class="form-group row">
                                        <div class="col-md-4">
                                            <span>{{ Form::label('name', 'Name') }}</span>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="form-group position-relative has-icon-left input-divider-left">
                                                {{ Form::text('name', null, ['id' => 'name', 'class' => 'form-control round', 'placeholder' => 'Brand name', 'autofocus', 'required']) }}
                                                <div class="form-control-position">
                                                    <i class="feather icon-cpu"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="form-group row">
                                        <div class="col-md-4">
                                            <span>{{ Form::label('slug', 'Slug') }}</span>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="form-group position-relative has-icon-left input-divider-left">
                                                {{ Form::text('slug', null, ['id' => 'slug', 'class' => 'form-control round', 'placeholder' => 'Input slug']) }}
                                                <div class="form-control-position">
                                                    <i class="feather icon-link"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="form-group row">
                                        <div class="col-md-4">
                                            <span>{{ Form::label('description', 'Description') }}</span>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="form-group position-relative has-icon-left input-divider-left">
                                                {{ Form::tel('description', null, ['id' => 'description', 'class' => 'form-control round', 'placeholder' => 'Input description']) }}
                                                <div class="form-control-position">
                                                    <i class="feather icon-file-plus"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="form-group row">
                                        <div class="col-md-4">
                                            <span>{{ Form::label('image', 'Logo') }}</span>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="form-group position-relative has-icon-left input-divider-left">
                                                {{ Form::file('image', ['id' => 'image', 'class' => 'form-control round', 'placeholder' => 'Upload Logo', 'accept' => '.png, .gif']) }}
                                                <div class="form-control-position">
                                                    <i class="feather icon-image"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="divider">
                                <div class="divider-text"></div>
                            </div>
                            <div class="row">
                                <div class="col-12 text-center">
                                    {!! Form::button('Submit', ['type' => 'submit', 'class' => 'btn btn-primary mr-1 mb-1 waves-effect waves-light']) !!}
                                    {!! Form::button('Reset', ['type' => 'reset', 'class' => 'btn btn-outline-warning mr-1 mb-1 waves-effect waves-light']) !!}
                                    {!! Form::button('Cancel', ['id' => 'cancel', 'class' => 'btn btn-warning mr-1 mb-1 waves-effect waves-light']) !!}
                                </div>
                            </div>
                        </div>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script type="text/javascript">
        $(document).ready(function () {
            $("#create").click(function(){
                $(".create").show("slow");
                $(".listView").hide("slow");
                $(".card-header h4").html("Add new Brand")
            });
            $("#cancel").click(function(){
                $(".create").hide("slow");
                $(".listView").show("slow");
                $(".card-header h4").html("Brand List")
            });
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $(".change-status-active").mouseover(function () {
                $(this).children("i").removeClass('icon-check-circle').addClass('icon-refresh-cw');
            }).mouseout(function () {
                $(this).children("i").addClass('icon-check-circle').removeClass('icon-refresh-cw');
            });
            $(".change-status-inactive").mouseover(function () {
                $(this).children("i").addClass('icon-refresh-cw').removeClass('icon-x-circle');
            }).mouseout(function () {
                $(this).children("i").addClass('icon-x-circle').removeClass('icon-refresh-cw');
            })
        })
    </script>

    <script src="{{ asset('Default/app-assets/vendors/js/forms/validation/jqBootstrapValidation.js') }}"></script>
    <script src="{{ asset('Default/app-assets/js/scripts/forms/validation/form-validation.js') }}"></script>
    <script src="{{ asset('Default/app-assets/vendors/js/pickers/pickadate/picker.js') }}"></script>
    <script src="{{ asset('Default/app-assets/vendors/js/pickers/pickadate/picker.date.js') }}"></script>
    <script src="{{ asset('Default/app-assets/js/scripts/pickers/dateTime/pick-a-datetime.js') }}"></script>
@stop
