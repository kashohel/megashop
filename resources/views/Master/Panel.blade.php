<!DOCTYPE html>
<html class="loaded" lang="en" data-textdirection="ltr"><!-- BEGIN: Head-->
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description"
          content="Advanced Super Shop management system with full inventory and full profit calculation with various income and expense fields.">
    <meta name="keywords"
          content="super shop, mega, inventory, pos, e-commerce, purchase, sale, report, profit, loss, online, best, software">
    <meta name="author" content="SOKRIYO">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ $title??"[|]" }} :|: {{ __(config('app.name')) }}</title>

    <link rel="apple-touch-icon" href="{{ asset('/Vuesax/img/favicon.ico') }}">
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('/Vuesax/img/favicon.ico') }}">
    {{--    <link rel="stylesheet" type="text/css" href="{{ asset('/css/app.css') }}">--}}
    <link href="{{ asset('/Vuesax/css/custom.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('/Vuesax/css/vendors.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/Vuesax/css/apexcharts.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/Vuesax/css/tether-theme-arrows.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/Vuesax/css/tether.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/Vuesax/css/shepherd-theme-default.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/Vuesax/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/Vuesax/css/bootstrap-extended.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/Vuesax/css/colors.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/Vuesax/css/components.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/Vuesax/css/dark-layout.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/Vuesax/css/semi-dark-layout.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/Vuesax/css/vertical-menu.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/Vuesax/css/palette-gradient.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/Vuesax/css/tour.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/Vuesax/css/style.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/Default/app-assets/vendors/css/extensions/toastr.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/Default/app-assets/css/plugins/extensions/toastr.css') }}">
    @yield('style')
    <script src="{{ asset('/Vuesax/js/feather.min.js') }}"></script>
</head>

<body class="vertical-layout vertical-menu-modern 2-columns  navbar-floating footer-static"
      data-open="click" data-menu="vertical-menu-modern" data-col="2-columns">

{{--<div id="panel">--}}
<!-- BEGIN: Main Menu-->
<div class="main-menu menu-fixed menu-light menu-accordion menu-shadow" data-scroll-to-active="true">
    <div class="navbar-header">
        <ul class="nav navbar-nav flex-row">
            <li class="nav-item mr-auto"><a class="navbar-brand">
                    <div class="brand-logo"></div>
                    <h2 class="brand-text mb-0">{{ __(config('app.name')) }}</h2></a></li>
            <li class="nav-item nav-toggle"><a class="nav-link modern-nav-toggle pr-0 shepherd-modal-target"
                                               data-toggle="collapse"><i
                        class="icon-x d-block d-xl-none font-medium-4 primary toggle-icon feather icon-disc"></i><i
                        class="toggle-icon icon-disc font-medium-4 d-none d-xl-block collapse-toggle-icon primary feather"
                        data-ticon="icon-disc" tabindex="0"></i></a></li>
        </ul>
    </div>
    <div class="shadow-bottom"></div>
    <div class="main-menu-content">
        <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
            @include('Elements.Menu')
        </ul>
    </div>
</div>
<!-- END: Main Menu-->

<!-- BEGIN: Content-->
<div class="app-content content">

    <!-- BEGIN: Header-->
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <nav class="header-navbar navbar-expand-lg navbar navbar-with-menu floating-nav navbar-light navbar-shadow">
        <div class="navbar-wrapper">
            <div class="navbar-container content">
                <div class="navbar-collapse" id="navbar-mobile">
                    <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                        <ul class="nav navbar-nav">
                            <li class="nav-item mobile-menu d-xl-none mr-auto"><a
                                    class="nav-link nav-menu-main menu-toggle hidden-xs is-active"
                                    href="#"><i
                                        class="ficon feather icon-menu"></i></a></li>
                        </ul>
                        <ul class="nav navbar-nav bookmark-icons">
                            <!-- li.nav-item.mobile-menu.d-xl-none.mr-auto-->
                            <!--   a.nav-link.nav-menu-main.menu-toggle.hidden-xs(href='#')-->
                            <!--     i.ficon.feather.icon-menu-->
                            <li class="nav-item d-none d-lg-block"><a class="nav-link"
                                                                      href="#"
                                                                      data-toggle="tooltip" data-placement="top"
                                                                      title="" data-original-title="Todo"><i
                                        class="ficon feather icon-check-square"></i></a></li>
                            <li class="nav-item d-none d-lg-block"><a class="nav-link"
                                                                      href="#"
                                                                      data-toggle="tooltip" data-placement="top"
                                                                      title="" data-original-title="Chat"><i
                                        class="ficon feather icon-message-square"></i></a></li>
                            <li class="nav-item d-none d-lg-block"><a class="nav-link"
                                                                      href="#"
                                                                      data-toggle="tooltip" data-placement="top"
                                                                      title="" data-original-title="Email"><i
                                        class="ficon feather icon-mail"></i></a></li>
                            <li class="nav-item d-none d-lg-block"><a class="nav-link"
                                                                      href="#"
                                                                      data-toggle="tooltip" data-placement="top"
                                                                      title="" data-original-title="Calendar"><i
                                        class="ficon feather icon-calendar"></i></a></li>
                        </ul>
                        <ul class="nav navbar-nav">
                            <li class="nav-item d-none d-lg-block"><a class="nav-link bookmark-star"><i
                                        class="ficon feather icon-star warning"></i></a>
                                <div class="bookmark-input search-input">
                                    <div class="bookmark-input-icon"><i class="feather icon-search primary"></i>
                                    </div>
                                    <input class="form-control input" type="text" placeholder="Explore Vuesax..."
                                           tabindex="0" data-search="template-list">
                                    <ul class="search-list show">
                                        <li class="auto-suggestion d-flex align-items-center justify-content-between cursor-pointer">
                                            <a class="d-flex align-items-center justify-content-between w-100"
                                               href="#">
                                                <div class="d-flex justify-content-start"><span class="mr-75 ft-home"
                                                                                                data-icon="ft-home"></span><span>Analytics Dashboard</span>
                                                </div>
                                            </a></li>
                                        <li class="auto-suggestion d-flex align-items-center justify-content-between cursor-pointer">
                                            <a class="d-flex align-items-center justify-content-between w-100"
                                               href="#">
                                                <div class="d-flex justify-content-start"><span class="mr-75 ft-info"
                                                                                                data-icon="ft-info"></span><span>Alerts Component</span>
                                                </div>
                                            </a></li>
                                        <li class="auto-suggestion d-flex align-items-center justify-content-between cursor-pointer">
                                            <a class="d-flex align-items-center justify-content-between w-100"
                                               href="#">
                                                <div class="d-flex justify-content-start"><span class="mr-75 ft-user"
                                                                                                data-icon="ft-user"></span><span>Avatar</span>
                                                </div>
                                            </a></li>
                                        <li class="auto-suggestion d-flex align-items-center justify-content-between cursor-pointer">
                                            <a class="d-flex align-items-center justify-content-between w-100"
                                               href="#">
                                                <div class="d-flex justify-content-start"><span
                                                        class="mr-75 ft-bar-chart"
                                                        data-icon="ft-bar-chart"></span><span>Apex Chart</span></div>
                                            </a></li>
                                        <li class="auto-suggestion d-flex align-items-center justify-content-between cursor-pointer">
                                            <a class="d-flex align-items-center justify-content-between w-100"
                                               href="#">
                                                <div class="d-flex justify-content-start"><span class="mr-75 ft-home"
                                                                                                data-icon="ft-home"></span><span>eCommerce Dashboard</span>
                                                </div>
                                            </a></li>
                                        <li class="auto-suggestion d-flex align-items-center justify-content-between cursor-pointer">
                                            <a class="d-flex align-items-center justify-content-between w-100"
                                               href="#">
                                                <div class="d-flex justify-content-start"><span class="mr-75 ft-mail"
                                                                                                data-icon="ft-mail"></span><span>Email</span>
                                                </div>
                                            </a></li>
                                        <li class="auto-suggestion d-flex align-items-center justify-content-between cursor-pointer current_item">
                                            <a class="d-flex align-items-center justify-content-between w-100"
                                               href="#">
                                                <div class="d-flex justify-content-start"><span
                                                        class="mr-75 ft-message-square"
                                                        data-icon="ft-message-square"></span><span>Chat</span></div>
                                            </a></li>
                                        <li class="auto-suggestion d-flex align-items-center justify-content-between cursor-pointer">
                                            <a class="d-flex align-items-center justify-content-between w-100"
                                               href="#">
                                                <div class="d-flex justify-content-start"><span
                                                        class="mr-75 ft-calendar" data-icon="ft-calendar"></span><span>Calendar</span>
                                                </div>
                                            </a></li>
                                        <li class="auto-suggestion d-flex align-items-center justify-content-between cursor-pointer">
                                            <a class="d-flex align-items-center justify-content-between w-100"
                                               href="#">
                                                <div class="d-flex justify-content-start"><span class="mr-75 ft-list"
                                                                                                data-icon="ft-list"></span><span>Data List - List View</span>
                                                </div>
                                            </a></li>
                                        <li class="auto-suggestion d-flex align-items-center justify-content-between cursor-pointer">
                                            <a class="d-flex align-items-center justify-content-between w-100"
                                               href="#">
                                                <div class="d-flex justify-content-start"><span class="mr-75 ft-image"
                                                                                                data-icon="ft-image"></span><span>Data List - Thumb View</span>
                                                </div>
                                            </a></li>
                                    </ul>
                                </div>
                                <!-- select.bookmark-select-->
                                <!--   option Chat-->
                                <!--   option email-->
                                <!--   option todo-->
                                <!--   option Calendar-->
                            </li>
                        </ul>
                    </div>
                    <ul class="nav navbar-nav float-right">
                        <li class="dropdown dropdown-language nav-item"><a class="dropdown-toggle nav-link"
                                                                           id="dropdown-flag"
                                                                           data-toggle="dropdown"
                                                                           aria-haspopup="true"
                                                                           aria-expanded="false"><i
                                    class="ficon feather icon-target"></i><span
                                    class="selected-language">{{ __('menu.language') }}</span></a>
                            <div class="dropdown-menu" aria-labelledby="dropdown-flag"><a class="dropdown-item"
                                                                                          href="{{ route('lan', 'en') }}"
                                                                                          data-language="en"><i
                                        class="flag-icon flag-icon-us"></i>  {{ __('menu.english') }}</a><a class="dropdown-item"
                                                                                          href="{{ route('lan', 'bn') }}"
                                                                                          data-language="fr"><i
                                        class="flag-icon flag-icon-bd"></i> {{ __('menu.bangla') }}</a></div>
                        </li>
                        <li class="nav-item d-none d-lg-block"><a class="nav-link nav-link-expand"><i
                                    class="ficon feather icon-maximize"></i></a></li>
                        <li class="nav-item nav-search"><a class="nav-link nav-link-search"><i
                                    class="ficon feather icon-search"></i></a>
                            <div class="search-input">
                                <div class="search-input-icon"><i class="feather icon-search primary"></i></div>
                                <input class="input" type="text" placeholder="Explore Vuesax..." tabindex="-1"
                                       data-search="template-list">
                                <div class="search-input-close"><i class="feather icon-x"></i></div>
                                <ul class="search-list"></ul>
                            </div>
                        </li>
                        <li class="dropdown dropdown-notification nav-item"><a class="nav-link nav-link-label"
                                                                               href="#"
                                                                               data-toggle="dropdown"><i
                                    class="ficon feather icon-bell"></i><span
                                    class="badge badge-pill badge-primary badge-up">5</span></a>
                            <ul class="dropdown-menu dropdown-menu-media dropdown-menu-right">
                                <li class="dropdown-menu-header">
                                    <div class="dropdown-header m-0 p-2">
                                        <h3 class="white">5 New</h3><span
                                            class="notification-title">App Notifications</span>
                                    </div>
                                </li>
                                <li class="scrollable-container media-list ps"><a
                                        class="d-flex justify-content-between"
                                        href="javascript:void(0)">
                                        <div class="media d-flex align-items-start">
                                            <div class="media-left"><i
                                                    class="feather icon-plus-square font-medium-5 primary"></i>
                                            </div>
                                            <div class="media-body">
                                                <h6 class="primary media-heading">You have new order!</h6>
                                                <small class="notification-text"> Are your going to meet me tonight?
                                                </small>
                                            </div>
                                            <small>
                                                <time class="media-meta" datetime="2015-06-11T18:29:20+08:00">9
                                                    hours
                                                    ago
                                                </time>
                                            </small>
                                        </div>
                                    </a><a class="d-flex justify-content-between" href="javascript:void(0)">
                                        <div class="media d-flex align-items-start">
                                            <div class="media-left"><i
                                                    class="feather icon-download-cloud font-medium-5 success"></i>
                                            </div>
                                            <div class="media-body">
                                                <h6 class="success media-heading red darken-1">99% Server load</h6>
                                                <small class="notification-text">You got new order of goods.</small>
                                            </div>
                                            <small>
                                                <time class="media-meta" datetime="2015-06-11T18:29:20+08:00">5 hour
                                                    ago
                                                </time>
                                            </small>
                                        </div>
                                    </a><a class="d-flex justify-content-between" href="javascript:void(0)">
                                        <div class="media d-flex align-items-start">
                                            <div class="media-left"><i
                                                    class="feather icon-alert-triangle font-medium-5 danger"></i>
                                            </div>
                                            <div class="media-body">
                                                <h6 class="danger media-heading yellow darken-3">Warning
                                                    notifixation</h6>
                                                <small class="notification-text">Server have 99% CPU usage.</small>
                                            </div>
                                            <small>
                                                <time class="media-meta" datetime="2015-06-11T18:29:20+08:00">Today
                                                </time>
                                            </small>
                                        </div>
                                    </a><a class="d-flex justify-content-between" href="javascript:void(0)">
                                        <div class="media d-flex align-items-start">
                                            <div class="media-left"><i
                                                    class="feather icon-check-circle font-medium-5 info"></i></div>
                                            <div class="media-body">
                                                <h6 class="info media-heading">Complete the task</h6>
                                                <small class="notification-text">Cake sesame snaps cupcake</small>
                                            </div>
                                            <small>
                                                <time class="media-meta" datetime="2015-06-11T18:29:20+08:00">Last
                                                    week
                                                </time>
                                            </small>
                                        </div>
                                    </a><a class="d-flex justify-content-between" href="javascript:void(0)">
                                        <div class="media d-flex align-items-start">
                                            <div class="media-left"><i
                                                    class="feather icon-file font-medium-5 warning"></i></div>
                                            <div class="media-body">
                                                <h6 class="warning media-heading">Generate monthly report</h6>
                                                <small class="notification-text">Chocolate cake oat cake tiramisu
                                                    marzipan
                                                </small>
                                            </div>
                                            <small>
                                                <time class="media-meta" datetime="2015-06-11T18:29:20+08:00">Last
                                                    month
                                                </time>
                                            </small>
                                        </div>
                                    </a>
                                    <div class="ps__rail-x">
                                        <div class="ps__thumb-x" tabindex="0"></div>
                                    </div>
                                    <div class="ps__rail-y">
                                        <div class="ps__thumb-y" tabindex="0"></div>
                                    </div>
                                </li>
                                <li class="dropdown-menu-footer"><a class="dropdown-item p-1 text-center"
                                                                    href="javascript:void(0)">Read all
                                        notifications</a>
                                </li>
                            </ul>
                        </li>
                        <li class="dropdown dropdown-user nav-item"><a
                                class="dropdown-toggle nav-link dropdown-user-link"
                                href="#"
                                data-toggle="dropdown">
                                <div class="user-nav d-sm-flex d-none"><span
                                        class="user-name text-bold-600">{{ Auth::guard('staff')->user()->first_name.' '.Auth::guard('staff')->user()->last_name }}</span><span class="user-status">{{ Auth::guard('staff')->user()->rule->name??"Super User" }}</span>
                                </div>
                                <span>
                                    @if (Auth::guard('staff')->user()->avatar)
                                        <img class="round" src="{{ asset('/storage/Staff/'.Auth::guard('staff')->user()->avatar) }}" alt="avatar" height="40" width="40">
                                    @else
                                        <img class="round" src="{{ asset('/Vuesax/img/avatar-s-11.png') }}" alt="avatar" height="40" width="40">
                                    @endif
                                </span></a>
                            <div class="dropdown-menu dropdown-menu-right"><a class="dropdown-item"
                                                                              href="#"><i
                                        class="feather icon-user"></i> Edit Profile</a><a class="dropdown-item"
                                                                                          href="#"><i
                                        class="feather icon-mail"></i> My Inbox</a><a class="dropdown-item"
                                                                                      href="#"><i
                                        class="feather icon-check-square"></i> Task</a><a class="dropdown-item"
                                                                                          href="#"><i
                                        class="feather icon-message-square"></i> Chats</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item"
                                   href="{{ route('logout') }}"><i
                                        class="feather icon-power"></i> Logout</a>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </nav>
    <!-- END: Header-->

    <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body">
            <!-- Dashboard Analytics Start -->
        @yield('body')
        <!-- Dashboard Analytics end -->
        </div>
    </div>
</div>
<!-- END: Content-->


{{--        @include('Elements.Customizer')--}}
{{--        @include('Elements.FootButton', ['data' => "Under Developing"])--}}

<div class="sidenav-overlay"></div>
<div class="drag-target"></div>
{{--</div>--}}
@include('Elements.Footer')
{{--<script src="{{ asset('/js/app.js') }}"></script>--}}
<script src="{{ asset('/Vuesax/js/vendors.min.js') }}"></script>
<script src="{{ asset('/Vuesax/js/apexcharts.min.js') }}"></script>
<script src="{{ asset('/Vuesax/js/tether.min.js') }}"></script>
<script src="{{ asset('/Vuesax/js/shepherd.min.js') }}"></script>
<script src="{{ asset('/Vuesax/js/app-menu.min.js') }}"></script>
<script src="{{ asset('/Vuesax/js/app.min.js') }}"></script>
<script src="{{ asset('/Vuesax/js/components.min.js') }}"></script>
<script src="{{ asset('/Vuesax/js/customizer.min.js') }}"></script>
<script src="{{ asset('/Vuesax/js/footer.min.js') }}"></script>
<script src="{{ asset('Default/app-assets/vendors/js/extensions/toastr.min.js') }}"></script>
<script src="{{ asset('Default/app-assets/js/scripts/extensions/toastr.js') }}"></script>
@php($do = "Hello jan")
<script type="text/javascript">
    @if($errors->any())
    @foreach ($errors->all() as $error)
    toastr.error('{{ $error }}', 'Process Error', {positionClass: 'toast-bottom-right', containerId: 'toast-bottom-right'});
    @endforeach
    @endif
    @if(\Illuminate\Support\Facades\Session::has('message.error'))
    toastr.error('{{ \Illuminate\Support\Facades\Session::pull('message.error') }}', 'Process Error', {positionClass: 'toast-bottom-right', containerId: 'toast-bottom-right'});
    @endif
    @if(\Illuminate\Support\Facades\Session::get('message.success'))
    toastr.success('{{ \Illuminate\Support\Facades\Session::pull('message.success') }}', 'Process Successful', {positionClass: 'toast-bottom-right', containerId: 'toast-bottom-right'});
    @endif
    @if(\Illuminate\Support\Facades\Session::get('message.warning'))
    toastr.warning('{{ \Illuminate\Support\Facades\Session::pull('message.warning') }}', 'Warning message', {positionClass: 'toast-bottom-right', containerId: 'toast-bottom-right'});
    @endif
    @if(\Illuminate\Support\Facades\Session::get('message.info'))
    toastr.info('{{ \Illuminate\Support\Facades\Session::pull('message.info') }}', '', {positionClass: 'toast-bottom-right', containerId: 'toast-bottom-right'});
    @endif
</script>
@yield('script')
</body>
</html>

