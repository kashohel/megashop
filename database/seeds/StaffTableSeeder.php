<?php

use App\Model\Staff;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class StaffTableSeeder extends Seeder
{
    public function run()
    {

        Staff::truncate();
        Staff::create([
            'code' => uniqid(),
            'first_name' => "Innovation",
            'last_name' => "Squad",
            'mobile' => '01749999700',
            'phone' => '01834411000',
            'email' => 'alam@foxof.org',
            'gender' => 1,
            'designation' => 'Administrator',
            'joining_date' => Carbon::today(),
            'username' => 'admin',
            'password' => Hash::make('12345678'),
            'active' => 1,
        ]);
    }
}
