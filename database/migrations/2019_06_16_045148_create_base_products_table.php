<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBaseProductsTable extends Migration
{
    public function up()
    {
        Schema::create('base_products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->tinyInteger('type'); // Digital, Standard, Service, Combo, Affiliate
            $table->string('name', 100);
            $table->string('sub_name', 100)->nullable();
            $table->boolean('variation')->default(0);
            $table->boolean('unique_serial')->default(0);
            $table->unsignedInteger('category_id');
            $table->unsignedInteger('subcategory_id')->nullable();
            $table->string('details', 2000)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::dropIfExists('base_products');
    }
}
