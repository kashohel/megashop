<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGroupPricesTable extends Migration
{
    public function up()
    {
        Schema::create('group_prices', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('price_group_id');
            $table->double('price', 10,2);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::dropIfExists('group_prices');
    }
}
