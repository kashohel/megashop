<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            /* $table->foreign('base_product_id')->references('base_products')->on('id'); */
            $table->integer('base_product_id');
            $table->string('code', 100);

            /*
            $table->foreign('variant')->references('product_variants')->on('id')->onDelete('set null');
            $table->foreign('sub_variant')->references('product_variants')->on('id')->onDelete('set null');
            */

            $table->integer('variant')->nullable();
            $table->integer('sub_variant')->nullable();

            $table->unsignedDecimal('cost')->nullable();
            $table->unsignedDecimal('price')->nullable();

            $table->integer('quantity')->nullable();
            $table->integer('alert_quantity')->nullable();

            /*
            $table->foreign('unit_id')->references('units')->on('id');
            $table->foreign('purchase_unit')->references('units')->on('id');
            $table->foreign('pos_unit')->references('units')->on('id');
            $table->foreign('shop_unit')->references('units')->on('id');
            */
            $table->integer('unit_id'); // Without relation
// Input field is mandatory but fill up with default unit
            $table->integer('purchase_unit');
            $table->integer('pos_unit');
            $table->integer('shop_unit');

            $table->integer('tax')->nullable(); // Null or percent %
            $table->tinyInteger('tax_type')->nullable(); // Include=1 Exclude = 0

            $table->boolean('shop_view')->default(0); // Hide=0 Show=1 Feature=2
            $table->boolean('pos_view')->default(1);

            $table->boolean('expiry')->default(0);
            $table->integer('expiry_day')->nullable();

            // Only for digital and affiliate product
            $table->string('file')->nullable();
            $table->string('link')->nullable();

            $table->decimal('length')->nullable();
            $table->decimal('width')->nullable();
            $table->decimal('height')->nullable();
            $table->decimal('weight')->nullable();
            $table->string('specification', 2000)->nullable();
            $table->string('image')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::dropIfExists('products');
    }
}
