<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStaffTable extends Migration
{
    public function up()
    {
        Schema::create('staff', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code', 20)->unique();
            $table->string('first_name', 60);
            $table->string('last_name', 60)->nullable();
            $table->string('mobile', 20)->nullable();
            $table->string('phone', 20)->nullable();
            $table->string('nid', 20)->nullable();
            $table->string('email', 100)->nullable();
            $table->integer('gender')->default(1);
            $table->string('marital_status', 20)->nullable();
            $table->date('birthday')->nullable();
            $table->string('address', 200)->nullable();
            $table->string('location_map', 20)->nullable();
            $table->string('designation', 100)->nullable();
            $table->integer('price_group')->nullable();
            $table->date('joining_date')->default(now());;
            $table->date('exempt_date')->nullable();
            $table->integer('rule_id')->nullable();
            $table->string('warehouse_id')->nullable();
            $table->string('details', 250)->nullable();
            $table->string('ip_address', 20)->nullable();
            $table->string('last_ip_address', 20)->nullable();
            $table->string('username', 20)->nullable();
            $table->string('password', 60)->nullable();
            $table->string('recovery_code', 100)->nullable();
            $table->date('recovery_expire')->nullable();
            $table->rememberToken();
            $table->dateTime('last_login')->nullable();
            $table->tinyInteger('active')->default(1);
            $table->string('avatar', 100)->nullable();
            $table->string('resume', 100)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::dropIfExists('staff');
    }
}
