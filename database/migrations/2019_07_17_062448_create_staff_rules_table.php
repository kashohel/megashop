<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStaffRulesTable extends Migration
{
    public function up()
    {
        Schema::create('staff_rules', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code', 32)->unique();
            $table->string('name', 60);
            $table->string('details')->nullable();
            $table->tinyInteger('products_index')->nullable()->default(0);
            $table->tinyInteger('products_add')->nullable()->default(0);
            $table->tinyInteger('products_edit')->nullable()->default(0);
            $table->tinyInteger('products_delete')->nullable()->default(0);
            $table->tinyInteger('products_cost')->nullable()->default(0);
            $table->tinyInteger('products_price')->nullable()->default(0);
            $table->tinyInteger('quotes_index')->nullable()->default(0);
            $table->tinyInteger('quotes_add')->nullable()->default(0);
            $table->tinyInteger('quotes_edit')->nullable()->default(0);
            $table->tinyInteger('quotes_pdf')->nullable()->default(0);
            $table->tinyInteger('quotes_email')->nullable()->default(0);
            $table->tinyInteger('quotes_delete')->nullable()->default(0);
            $table->tinyInteger('sales_index')->nullable()->default(0);
            $table->tinyInteger('sales_add')->nullable()->default(0);
            $table->tinyInteger('sales_pdf')->nullable()->default(0);
            $table->tinyInteger('sales_email')->nullable()->default(0);
            $table->tinyInteger('sales_delete')->nullable()->default(0);
            $table->tinyInteger('purchases_index')->nullable()->default(0);
            $table->tinyInteger('purchases_add')->nullable()->default(0);
            $table->tinyInteger('purchases_edit')->nullable()->default(0);
            $table->tinyInteger('purchases_pdf')->nullable()->default(0);
            $table->tinyInteger('purchases_email')->nullable()->default(0);
            $table->tinyInteger('purchases_delete')->nullable()->default(0);
            $table->tinyInteger('transfers_index')->nullable()->default(0);
            $table->tinyInteger('transfers_add')->nullable()->default(0);
            $table->tinyInteger('transfers_edit')->nullable()->default(0);
            $table->tinyInteger('transfers_pdf')->nullable()->default(0);
            $table->tinyInteger('transfers_email')->nullable()->default(0);
            $table->tinyInteger('transfers_delete')->nullable()->default(0);
            $table->tinyInteger('customers_index')->nullable()->default(0);
            $table->tinyInteger('customers_add')->nullable()->default(0);
            $table->tinyInteger('customers_edit')->nullable()->default(0);
            $table->tinyInteger('customers_delete')->nullable()->default(0);
            $table->tinyInteger('suppliers_index')->nullable()->default(0);
            $table->tinyInteger('suppliers_add')->nullable()->default(0);
            $table->tinyInteger('suppliers_edit')->nullable()->default(0);
            $table->tinyInteger('suppliers_delete')->nullable()->default(0);
            $table->tinyInteger('sales_deliveries')->nullable()->default(0);
            $table->tinyInteger('sales_add_delivery')->nullable()->default(0);
            $table->tinyInteger('sales_edit_delivery')->nullable()->default(0);
            $table->tinyInteger('sales_delete_delivery')->nullable()->default(0);
            $table->tinyInteger('sales_email_delivery')->nullable()->default(0);
            $table->tinyInteger('sales_pdf_delivery')->nullable()->default(0);
            $table->tinyInteger('sales_gift_cards')->nullable()->default(0);
            $table->tinyInteger('sales_add_gift_card')->nullable()->default(0);
            $table->tinyInteger('sales_edit_gift_card')->nullable()->default(0);
            $table->tinyInteger('sales_delete_gift_card')->nullable()->default(0);
            $table->tinyInteger('pos_index')->nullable()->default(0);
            $table->tinyInteger('sales_return_sales')->nullable()->default(0);
            $table->tinyInteger('reports_index')->nullable()->default(0);
            $table->tinyInteger('reports_warehouse_stock')->nullable()->default(0);
            $table->tinyInteger('reports_quantity_alerts')->nullable()->default(0);
            $table->tinyInteger('reports_expiry_alerts')->nullable()->default(0);
            $table->tinyInteger('reports_products')->nullable()->default(0);
            $table->tinyInteger('reports_daily_sales')->nullable()->default(0);
            $table->tinyInteger('reports_monthly_sales')->nullable()->default(0);
            $table->tinyInteger('reports_sales')->nullable()->default(0);
            $table->tinyInteger('reports_payments')->nullable()->default(0);
            $table->tinyInteger('reports_purchases')->nullable()->default(0);
            $table->tinyInteger('reports_profit_loss')->nullable()->default(0);
            $table->tinyInteger('reports_customers')->nullable()->default(0);
            $table->tinyInteger('reports_suppliers')->nullable()->default(0);
            $table->tinyInteger('reports_staff')->nullable()->default(0);
            $table->tinyInteger('reports_register')->nullable()->default(0);
            $table->tinyInteger('sales_payments')->nullable()->default(0);
            $table->tinyInteger('purchases_payments')->nullable()->default(0);
            $table->tinyInteger('purchases_expenses')->nullable()->default(0);
            $table->tinyInteger('products_adjustments')->nullable()->default(0);
            $table->tinyInteger('bulk_actions')->nullable()->default(0);
            $table->tinyInteger('customers_deposits')->nullable()->default(0);
            $table->tinyInteger('customers_delete_deposit')->nullable()->default(0);
            $table->tinyInteger('products_barcode')->nullable()->default(0);
            $table->tinyInteger('purchases_return_purchases')->nullable()->default(0);
            $table->tinyInteger('reports_expenses')->nullable()->default(0);
            $table->tinyInteger('reports_daily_purchases')->nullable()->default(0);
            $table->tinyInteger('reports_monthly_purchases')->nullable()->default(0);
            $table->tinyInteger('products_stock_count')->nullable()->default(0);
            $table->tinyInteger('edit_price')->nullable()->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::dropIfExists('staff_rules');
    }
}
