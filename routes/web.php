<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('{any}', 'Panel\PanelController@index')->where('any', '.*');
//Route::get('/{route?}', 'RouteController@show')->where('route', '([0-9]+(\/){0,1})*');
//Route::get('{vue_capture?}', 'Panel\PanelController@index')->where('vue_capture', '[\/\w\.-]*');

Route::get('/', function () { return view('welcome'); });
Route::get('about', function () { return view('welcome'); });
Route::get('access', 'Panel\PanelController@login')->name('login');
Route::post('access', 'Panel\PanelController@loginCheck')->name('loginCheck');
Route::get('access-recovery', 'Panel\PanelController@forgetPassword')->name('forgetPassword');
Route::post('access-recovery', 'Panel\PanelController@passwordRecovery')->name('forgetPassword');
Route::get('logout', 'Panel\PanelController@logout')->name('logout');
Route::get('lan/{code}', 'Panel\PanelController@switchLanguage')->name('lan');

Route::prefix('panel')->middleware('staff')->group(function (){
    Route::get('/', 'Panel\PanelController@index')->name('panel-dashboard');
    Route::get('shop', 'Panel\PanelController@ecommerce')->name('panel-shop');
    Route::resource('product', 'Panel\ProductController')->names('product');

    Route::prefix('setting')->group(function (){
        Route::get('/', 'Panel\PanelController@index')->name('setting');
        Route::resource('category', 'Panel\CategoryController')->names('category');
        Route::resource('brand', 'Panel\BrandController')->names('brand');
        Route::resource('unit', 'Panel\UnitController')->names('unit');
        Route::resource('staff', 'Panel\StaffController')->names('staff');
        Route::get('staff/{code}/status-change', 'Panel\StaffController@changeStatus')->name('staff.changeStatus');
        Route::resource('rule', 'Panel\StaffRuleController')->names('staff-rule');
        Route::get('rule/{code}/assigned', 'Panel\StaffRuleController@assigned')->name('staff-rule.assigned');
        Route::resource('supplier', 'Panel\SupplierController')->names('supplier');
        Route::resource('warehouse', 'Panel\WarehouseController')->names('warehouse');
        Route::resource('price-group', 'Panel\PriceGroupController')->names('priceGroup');
        Route::resource('customer', 'Panel\CustomerController')->names('customer');
        Route::resource('bank', 'Panel\BankController')->names('bank');
    });
});

//Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
