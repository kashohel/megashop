<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::get('warehouse-list', 'Panel\WarehouseController@apiList')->name('api.warehouse.list');
Route::get('warehouse-info/{code}', 'Panel\WarehouseController@apiSingle')->name('api.warehouse.info');
