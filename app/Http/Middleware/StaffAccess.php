<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class StaffAccess
{
    public function handle($request, Closure $next)
    {
        if (Auth::guard('staff')->check()){
            return $next($request);
        } else {
            return redirect(route('login'));
        }
    }
}
