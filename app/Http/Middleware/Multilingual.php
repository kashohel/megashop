<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Session;

class Multilingual
{
    public function handle($request, Closure $next)
    {
        if (Session::has('lang')){
            App::setLocale(Session::get('lang'));
        } else {
            App::setLocale(Config::get('app.locale'));
        }
        return $next($request);
    }
}
