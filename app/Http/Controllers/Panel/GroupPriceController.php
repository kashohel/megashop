<?php

namespace App\Http\Controllers\Panel;

use App\Model\GroupPrice;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GroupPriceController extends Controller
{
    public function index()
    {
        //
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show(GroupPrice $groupPrice)
    {
        //
    }

    public function edit(GroupPrice $groupPrice)
    {
        //
    }

    public function update(Request $request, GroupPrice $groupPrice)
    {
        //
    }

    public function destroy(GroupPrice $groupPrice)
    {
        //
    }
}
