<?php

namespace App\Http\Controllers\Panel;

use App\Model\Staff;
use App\Model\StaffRule;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class StaffRuleController extends Controller
{

    public function index()
    {
        $title = "Staff Role";
        $rules = StaffRule::all();
        return view('Panel.Default.StaffRule', compact('title', 'rules'));
    }


    public function store(Request $request)
    {
        $data['name'] = $request->name;
        $data['code'] = uniqid();
        $data['details'] = $request->details;
        try{
            StaffRule::create($data);
        } catch (\PDOException $e){
            Session::put(['message.error' => $e->getMessage()]);
            return back()->withInput();
        }
        Session::put(['message.success' => 'New staff role create successfully.']);
        return back();
    }


    public function show($code)
    {
        $title = 'Staff Rule view';
        $rule = StaffRule::where('code', $code)->first();
        if($rule) {
            return view('Panel.Default.StaffRuleShow', compact('title', 'rule', 'staffs'));
        } else {
            Session::put("message.error", "Link not working. Please refresh page and try again.");
            return back();
        }
    }

    public function assigned($code){
        $title = 'Staff under this rule';
        $rule = StaffRule::where('code', $code)->first();
        if(!$rule){
            Session::put("message.error", "Link not working. Please refresh page and try again.");
            return back();
        }
        $staffs = Staff::where('rule_id', $rule->id)->get();
        return view('Panel.Default.StaffRuleAssigned', compact('title', 'rule', 'staffs'));
    }


    public function edit($code)
    {
        $title = "Edit staff rule";
        $rule = StaffRule::where('code', $code)->first();
        if(!$rule){
            Session::put('message.error', 'Link not working. Please refresh page and try again.');
            return back();
        }

        $modules = [
            "Product" => [["products_index", "products_add", "products_edit", "products_delete"], ["products_cost" => 'Cost', "products_price" => 'Price']],
            "Sales" => [["sales_index", "sales_add", "sales_edit", "sales_delete"], ["sales_pdf" => 'PDF', "sales_email" => 'Email']],
//            "Sales" => [["sales_deliveries``sales_add_delivery``sales_edit_delivery``sales_delete_delivery``sales_email_delivery``sales_pdf_delivery``sales_gift_cards``sales_add_gift_card``sales_edit_gift_card``sales_delete_gift_card``pos_index``sales_return_sales``sales_payments"]],
            "Delivery" => [[], ["ffff"]],
            "Gift Card" => [[], ["ggg"]],
            "Quotations" => [["quotes_index", "quotes_add", "quotes_edit", "quotes_delete"], [ "quotes_pdf" => 'PDF', "quotes_email" => 'Email']],
            "Purchase" => [["purchases_index", "purchases_add", "purchases_edit", "purchases_delete"], ["purchases_pdf" => 'PDF', "purchases_email" => 'Email', "purchases_payments" => 'Payment', "purchases_expenses" => 'Expense']],
            "Transfers" => [["transfers_index", "transfers_add", "transfers_edit", "transfers_delete"], ["transfers_pdf" => 'PDF', "transfers_email" => 'Email']],
            "Return" => [[], ["gg"]],
            "Customer" => [["customers_index", "customers_add", "customers_edit", "customers_delete"], [""]],
            "Supplier" => [["suppliers_index", "suppliers_add", "suppliers_edit", "suppliers_delete"], []],
            "Report" => [[], ['reports_index' => 'Index', 'reports_warehouse_stock' => 'Warehouse Stock', 'reports_quantity_alerts' => 'Alert Qty.', 'reports_expiry_alerts' => 'Expiry Alert', 'reports_products' => 'Product', 'reports_daily_sales' => 'Daily Sale', 'reports_monthly_sales' => 'Monthly Sale', 'reports_sales' => 'Sales', 'reports_payments' => 'Payment', 'reports_purchases' => 'Purchase', 'reports_profit_loss' => 'Loss Profit', 'reports_customers' => 'customer', 'reports_suppliers' => 'supplier', 'reports_staff' => 'Staff', 'reports_register' => 'Register']],
            "Miscellaneous" => [[], ["ggg"]],
            // products_adjustments``bulk_actions``customers_deposits``customers_delete_deposit``products_barcode``purchases_return_purchases``reports_expenses``reports_daily_purchases``reports_monthly_purchases``products_stock_count``edit_price'
        ];
        return view('Panel.Default.StaffRuleEdit', compact('title', 'rule', 'modules'));
    }


    public function update(Request $request, $code)
    {
        $permission = ["products_index", "products_add", "products_edit", "products_delete", "products_cost", "products_price", "sales_index", "sales_add",
            "sales_edit", "sales_delete", "sales_pdf", "sales_email", "sales_deliveries", "sales_add_delivery", "sales_edit_delivery", "sales_delete_delivery",
        "sales_email_delivery", "sales_pdf_delivery", "sales_gift_cards", "sales_add_gift_card", "sales_edit_gift_card", "sales_delete_gift_card",
        "pos_index``sales_return_sales``sales_payments", "quotes_index", "quotes_add", "quotes_edit", "quotes_delete", "quotes_pdf", "quotes_email",
        "purchases_index", "purchases_add", "purchases_edit", "purchases_delete", "purchases_pdf", "purchases_email", "purchases_payments",
        "purchases_expenses", "transfers_index", "transfers_add", "transfers_edit", "transfers_delete", "transfers_pdf", "transfers_email",
        "customers_index", "customers_add", "customers_edit", "customers_delete", "suppliers_index", "suppliers_add", "suppliers_edit",
        "suppliers_delete", "reports_index", "reports_warehouse_stock", "reports_quantity_alerts", "reports_expiry_alerts", "reports_products",
        "reports_daily_sales", "reports_monthly_sales", "reports_sales", "reports_payments", "reports_purchases", "reports_profit_loss",
        "reports_customers", "reports_suppliers", "reports_staff", "reports_register" ,"products_adjustments", "bulk_actions", "customers_deposits",
        "customers_delete_deposit", "products_barcode", "purchases_return_purchases", "reports_expenses", "reports_daily_purchases", "reports_monthly_purchases", "products_stock_count", "edit_price"
         ];

        $rule = StaffRule::where('code', $code)->first();
        $request->validate([
            'name' => 'required'
        ]);
        $data['name'] = $request->name;
        $data['details'] = $request->details;
        foreach ($permission as $access){
            $data[$access] = $request->$access??0;
        }
        try{
            $rule->update($data);
        } catch (\PDOException $e){
            Session::put('message.error', $e);
            return back()->withInput();
        }
        Session::put('message.success', "Rule permission successfully update.");
        return back();
    }


    public function destroy($code)
    {
        $rule = StaffRule::where('code', $code)->first();
        if($rule){
            if($rule->assigned->count() > 0){
                Session::put('message.warning', "Rule assigned with staff can not delete. First change staff from this to other rule and try again.");
            } else {
                $rule->delete();
                Session::put('message.success', 'Staff Rule deleted successfully.');
            }
            return back();
        } else {
            Session::put('message.error', 'Staff rule can not find out. Reload page and try again.');
            return back();
        }
    }
}
