<?php

namespace App\Http\Controllers\Panel;

use App\Model\WarehouseStock;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class WarehouseStockController extends Controller
{
    public function index()
    {
        //
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }


    public function show(WarehouseStock $warehouseStock)
    {
        //
    }


    public function edit(WarehouseStock $warehouseStock)
    {
        //
    }


    public function update(Request $request, WarehouseStock $warehouseStock)
    {
        //
    }


    public function destroy(WarehouseStock $warehouseStock)
    {
        //
    }
}
