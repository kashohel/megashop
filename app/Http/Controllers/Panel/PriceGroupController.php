<?php

namespace App\Http\Controllers\Panel;

use App\Model\PriceGroup;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class PriceGroupController extends Controller
{
    public function index()
    {
        $title = "Price Group";
        $priceGroups = PriceGroup::all();
        return view('Panel.Default.PriceGroup', compact('title', 'priceGroups'));
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        if (!null == $request->name && $request->name != ""){
            try{
                PriceGroup::create(['code' => uniqid(), 'name' => $request->name]);
            } catch (\PDOException $e){
                Session::put(['message.error' => $e->getMessage()]);
                return back()->withInput();
            }
            Session::put(['message.success', 'New Price Group add successful.']);
            return back();
        } else {
            Session::put(['message.warning', 'Name can not be empty. Fill-up carefully']);
            return back()->withInput();
        }
    }

    public function show(PriceGroup $priceGroup)
    {
        //
    }

    public function edit(PriceGroup $priceGroup)
    {
        //
    }

    public function update(Request $request, PriceGroup $priceGroup)
    {
        //
    }

    public function destroy(Request $request)
    {
        dd($request);
    }
}
