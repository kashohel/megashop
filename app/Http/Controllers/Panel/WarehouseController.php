<?php

namespace App\Http\Controllers\Panel;

use App\Http\Requests\WarehouseStoreRequest;
use App\Model\PriceGroup;
use App\Model\Warehouse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class WarehouseController extends Controller
{
    public function index()
    {
        $title = "Warehouse";
        $warehouses = Warehouse::whereNotNull('id')->get();
        $priceGroup = PriceGroup::all();
        return view('Panel.Default.Warehouse', compact('title', 'warehouses', 'priceGroup'));
    }


    public function create()
    {
        //
    }


    public function store(Request $request)
    {
//        $validated = $request->validated();
//        dd($validated);

        $newWarehouse['code'] = uniqid();
        $newWarehouse['name'] = $request->name;
        $newWarehouse['phone'] = $request->phone;
        $newWarehouse['email'] = $request->email;
        $newWarehouse['map'] = $request->map_point;
        $newWarehouse['address'] = $request->address;
        if (!null == $request->price_group){
            $priceGroup = PriceGroup::where('code', $request->price_group)->first();
            if ($priceGroup){
                $newWarehouse['price_group'] = $priceGroup->id;
            } else {
                Session::put(['message.error' => 'Price group not exist. Reload page and select again.']);
                return back()->withInput();
            }
        }

        try{
            Warehouse::create($newWarehouse);
            Session::put(['message.success' => 'Warehouse create successful.']);
        } catch (\PDOException $e){
            Session::put(['message.error' => $e->getMessage()]);
            return back()->withInput();
        }
        return back();
    }


    public function show(Warehouse $warehouse)
    {
        //
    }


    public function edit(Warehouse $warehouse)
    {
        //
    }


    public function update(Request $request, Warehouse $warehouse)
    {
        //
    }


    public function destroy(Warehouse $warehouse)
    {
        //
    }

    public function apiList(){
        return Warehouse::all()->toJson();
    }
    public function apiSingle($code){
        return Warehouse::where('code', $code)->first(['name', 'phone', 'email', 'address', 'map'])->toJson();
    }
}
