<?php

namespace App\Http\Controllers\Panel;

use App\Model\Brand;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class BrandController extends Controller
{
    public function index()
    {
        $title = "Brand List";
        $brands = Brand::all();
        return view('Panel.Default.BrandList', compact('title', 'brands'));
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $request->validate([

        ]);

        $data['code'] = uniqid();
        $data['name'] = $request->name;
        $data['slug'] = $request->slug?Str::slug($request->slug):Str::slug($request->name);
        $data['details'] = $request->description;
        $data['logo'] = $request->logo;
        if($request->image){
            $data['logo'] = $data['code'].".".$request->file('image')->getClientOriginalExtension();
        }

        try{
            Brand::create($data);
        } catch (\PDOException $e){
            Session::put('message.error', $e);
            return back();
        }

        if ($request->image){
            Storage::disk('public')->putFileAs('Brand', $request->file('image'), $data['logo']);
        }
        Session::put('message.success', "New Brand added successfully.");
        return back();
    }

    public function show(Brand $brand)
    {
        //
    }

    public function edit(Brand $brand)
    {
        //
    }

    public function update(Request $request, Brand $brand)
    {
        //
    }

    public function destroy($code)
    {
        $brand = Brand::where('code', $code)->first();
        if ($brand){
            try{
                $brand->delete();
            } catch (\PDOException $e){
                Session::put('message.error', $e);
                return back();
            }
            if ($brand->logo){
                Storage::disk('public')->delete('Brand/'.$brand->logo);
            }
            Session::put('message.success', "Brand delete successfully.");
            return back();
        } else {
            Session::put('message.error', "This brand is not available. Please reload and try again.");
            return back();
        }
    }
}
