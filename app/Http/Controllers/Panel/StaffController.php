<?php

namespace App\Http\Controllers\Panel;

use App\Model\Company;
use App\Model\PriceGroup;
use App\Model\Staff;
use App\Model\StaffRule;
use App\Model\Warehouse;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;

class StaffController extends Controller
{

    public function index()
    {
        $title = "All Staffs";
        $staffs = Staff::all();
        $rules = StaffRule::whereNotNull('id')->get(['name', 'code']);
        $warehouses = Warehouse::all();
        $prices = PriceGroup::all();
        return view('Panel.Default.StaffList', compact('title', 'staffs', 'rules', 'warehouses', 'companies', 'prices'));
    }


    public function create()
    {
        $title = "Create new Staff";
        return view('', compact('title', 'roles'));
    }


    public function store(Request $request)
    {
        $request->validate([
            'first_name' => 'required',
            'last_name' => 'required',
            'mobile' => 'required',
            'nid' => 'nullable|integer',
            'gender' => 'required',
            'designation' => 'required',
        ]);


        $data['code'] = uniqid();
        $data['active'] = 1;
        $data['first_name'] = $request->first_name;
        $data['last_name'] = $request->last_name;
        $data['phone'] = $request->phone;
        $data['mobile'] = $request->mobile;
        $data['nid'] = $request->nid;
        $data['email'] = $request->email;
        $data['gender'] = $request->gender;
        $data['marital_status'] = $request->marital_status;
        $data['birthday'] = $request->birthday?Carbon::parse($request->birthday):null;
        $data['address'] = $request->address;
        $data['designation'] = $request->designation;
        $data['details'] = $request->details;
        $data['joining_date'] = $request->joining?Carbon::parse($request->joining):Carbon::today();
        if ($request->price_group)
            $data['price_group'] = PriceGroup::where('code', $request->price_group)->first()->id;
        if ($request->warehouse)
            $data['warehouse_id'] = Warehouse::where('code', $request->warehouse)->first()->id;
        if ($request->rule)
            $data['rule_id'] = StaffRule::where('code', $request->rule)->first()->id;
        if ($request->profile)
            $data['avatar'] = "PP".$data['code'].".".$request->file('profile')->getClientOriginalExtension();
        if ($request->cv)
            $data['resume'] = "CV".$data['code'].".".$request->file('cv')->getClientOriginalExtension();

        try{
            Staff::create($data);
        } catch (\PDOException $e){
            Session::put('message.error', $e);
            return back()->withInput();
        }

        if (isset($request->profile)) {
            Storage::disk('public')->putFileAs('Staff', $request->file('profile'), $data['avatar']);
        }
        if (isset($request->cv)) {
            Storage::disk('public')->putFileAs('Staff', $request->file('cv'), $data['resume']);
        }

        Session::put('message.success', "Staff create successful.");
        return back();
    }


    public function show($code)
    {
        dd($code);
    }


    public function edit($code)
    {
        $title = "Update Staff Information";
        $staff = Staff::where('code', $code)->first();
        if (!$staff){
            Session::put('message.error', "Staff information not match. Reload and try again.");
            return back();
        }
        $rules = StaffRule::whereNotNull('id')->get(['name', 'code']);
        $warehouses = Warehouse::all();
        $prices = PriceGroup::all();
        return view('Panel.Default.StaffEdit', compact('title', 'staff', 'rules', 'warehouses', 'companies', 'prices'));
    }


    public function update(Request $request, $code)
    {

        $request->validate([
            'profile' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'cv' => 'file|max:5000|mimes:pdf,docx,doc|max:2048',
        ]);

        $staff = Staff::where('code', $code)->first();
        if ($staff){
            $data['first_name'] = $request->first_name;
            $data['last_name'] = $request->last_name;
            $data['mobile'] = $request->mobile;
            $data['phone'] = $request->phone;
            $data['email'] = $request->email;
            $data['nid'] = $request->nid;
            $data['gender'] = $request->gender;
            $data['marital_status'] = $request->marital_status;
            $data['birthday'] = $request->birthday?Carbon::parse($request->birthday):null;
            $data['address'] = $request->address;
            $data['designation'] = $request->designation;
            $data['price_group'] = $request->price_group;
            $data['joining_date'] = $request->joining?Carbon::parse($request->joining):null;
            $data['details'] = $request->details;
            if($request->price_group)
                $data['price_group'] = PriceGroup::where('code', $request->price_group)->first()->id;
            else
                $data['price_group'] = null;
            if($request->warehouse)
                $data['warehouse_id'] = Warehouse::where('code', $request->warehouse)->first()->id;
            else
                $data['warehouse_id'] = null;
            if ($request->rule)
                $data['rule_id'] = StaffRule::where('code', $request->rule)->first()->id;
            else
                $data['rule_id'] = null;



            if ($request->profile){
                $data['avatar'] = "PP".$code.".".$request->file('profile')->getClientOriginalExtension();
                $oldProfile = "Staff/".$staff->avatar;
            }
            if ($request->cv){
                $data['avatar'] = "CV".$code.".".$request->file('cv')->getClientOriginalExtension();
                $oldResume = "Staff/".$staff->resume;
            }
            try{
                $staff->update($data);
            } catch (\PDOException $e){
                Session::put('message.error', $e->getMessage());
                return back()->withInput();
            }
            if (isset($request->profile)) {
                Storage::disk('public')->delete('Staff/'.$oldProfile);
                Storage::disk('public')->putFileAs('Staff', $request->file('profile'), $data['avatar']);
            }
            if (isset($request->cv)) {
                Storage::disk('public')->delete('Staff/'.$oldResume);
                Storage::disk('public')->putFileAs('Staff', $request->file('cv'), $data['resume']);
            }
            Session::put('message.success', "Staff information successfully updated.");
            return redirect(route('staff.show', $code));
        } else {
            Session::put('message.error', "Staff information can not find out. Please refresh page and try again.");
            return back()->withInput();
        }
    }


    public function changeStatus($code)
    {
        $staff = Staff::where('code', $code)->first();
//        dd($staff->active);
        if ($staff) {
            if ($staff->active) {
                $staff->update(['active' => 0]);
                Session::put('message.success', "Staff deactivate successfully.");
            } else {
                $staff->update(['active' => 1]);
                Session::put('message.success', "Staff activate successfully.");
            }
            return back();
        } else {
            Session::put('message.error', "Try to access unauthorised link. Please refresh and try again.");
            return back();
        }
    }


    public function destroy($code)
    {
        dd($code);
    }
}
