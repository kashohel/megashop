<?php

namespace App\Http\Controllers\Panel;

use App\Model\Staff;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class PanelController extends Controller
{
    public $backPage;

    public function index()
    {
        $title = 'Dashboard';
        return view('Panel.Default.PanelHome', compact('title'));
    }

    public function dashboard()
    {
        return "Hello Dashboard";
    }

    public function ecommerce()
    {
        $title = 'E-Commerce';
        return view('Panel.Default.Ecommerce', compact('title'));
    }

    public function login(){
        $this->backPage = url()->previous();
        if (Auth::guard('staff')->check()){
            return back();
        } else {
            return view('Panel.Default.Login');
        }
    }

    public function loginCheck(Request $request){
        $request->validate([
            'username' => 'required',
            'password' => 'required',
        ]);

//        dd(Auth::guard('staff')->id);
        $staff = Staff::where('username', $request->username)->orWhere('email', $request->username)->orWhere('phone', $request->username)->first();
        if (!null == $staff && ($staff->username == $request->username | $staff->email == $request->username | $staff->phone == $request->username)) {
           if (Hash::check($request->password, $staff->password)) {
                if ($staff->active == 1 && $staff->role != 10) {
                    $remember = $request->remember;
                    Auth::guard('staff')->attempt([
                        'username' => $request->username,
                        'password' => $request->password
                    ], $remember);
                    Session::put(['message.success' => "Login Successful"]);
                } elseif ($staff->role == 0) {
                    Session::put(['message.error' => 'Your panel access downgraded.']);
                    return back()->withInput();
                } else {
                    Session::put(['message.error' => "User is inactive."]);
                    return back()->withInput();
                }
            } else {
                Session::put(['message.error' => "Password not matching. Input carefully"]);
                return back()->withInput();
            }
        } else {
            Session::put(['message.error' => "Username not matching."]);
            return back()->withInput();
        }
        if (isset($this->backPage) && !null == $this->backPage) {
            return redirect($this->backPage);
        } else {
            return redirect(route('panel-dashboard'));
        }


    }

    public function forgetPassword (){
        dd("Forget password form");
    }

    public function switchLanguage($code){
        Session::put('lang', $code);
        App::getLocale();
        return back();
    }

    public function passwordRecovery(Request $request){
        dd($request->all());
    }

    public function logout(){
        Auth::guard('staff')->logout();
        Session::flush();
        return redirect('/');
    }
}
