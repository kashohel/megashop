<?php

namespace App\Http\Controllers\Panel;

use App\Model\Unit;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UnitController extends Controller
{

    public function index()
    {
        //
    }


    public function create()
    {
        //
    }


    public function store(Request $request)
    {
        //
    }


    public function show(Unit $unit)
    {
        //
    }


    public function edit(Unit $unit)
    {
        //
    }


    public function update(Request $request, Unit $unit)
    {
        //
    }


    public function destroy(Unit $unit)
    {
        //
    }
}
