<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class WarehouseStoreRequest extends FormRequest
{

    public function authorize()
    {
        return false;
    }

    public function rules()
    {
        return [
            'name' => 'required|string',
            'email' => 'required|email',
            'phone' => 'required|integer|min:10|unique:warehouse',
            'address' => 'required|'
        ];
    }


    public function messages()
    {
        return [
            'name.required' => 'name is required!',
            'name.string' => 'Input valid name',
            'phone.required' => 'Phone number is required!',
            'phone.unique' => 'This phone number already used!',
            'phone.min' => 'Phone number must contain 11 digits!',
            'address.required' => 'Address can not be empty!'
        ];
    }
}
