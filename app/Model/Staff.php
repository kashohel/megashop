<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Auth\Authenticatable as AuthenticableTrait;

class Staff extends Model implements Authenticatable
{
    use AuthenticableTrait;
    use SoftDeletes;

    protected $fillable = [
        'code', 'first_name', 'last_name', 'mobile', 'phone', 'nid', 'email', 'gender', 'marital_status', 'birthday', 'address', 'location_map',
        'designation', 'price_group', 'joining_date', 'exempt_date', 'rule_id', 'warehouse_id', 'details',
        'ip_address', 'last_ip_address', 'username', 'password', 'recovery_code', 'recovery_expire', 'remember_token', 'last_login', 'active', 'avatar', 'resume'
    ];

    public function rule()
    {
        return $this->hasOne('App\Model\StaffRule', 'id', 'rule_id');
    }

    public function warehouse()
    {
        return $this->hasOne('App\Model\Warehouse', 'id', 'warehouse_id');
    }

    public function priceGroup()
    {
        return $this->hasOne('App\Model\PriceGroup', 'id', 'price_group');
    }
}
// exempt_date is nullable and is not null is equal not active this staff
//Only user who have rule_id can have login access. designation will be fill-up from role else manual.
//TODO: change column and make more related and meaningful column for this model
