<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Unit extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'code', 'name', 'base_unit', 'operator', 'unit_value', 'operation_value'
    ];

    protected $hidden = [
        //'password', 'remember_token',
    ];
    protected $casts = [
        //'email_verified_at' => 'datetime',
    ];
}
