<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class GroupPrice extends Model
{
    use SoftDeletes;
    protected $fillable = ['price_group_id', 'price'];

    public function group()
    {
        return $this->belongsTo('App\Model\PriceGroup', 'price_group_id', 'id');
    }
}
