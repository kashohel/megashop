<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BaseProduct extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'type', 'name', 'sub_name', 'variation', 'unique_serial', 'category_id', 'subcategory_id', 'details'
    ];

    protected $hidden = [
        //'password', 'remember_token',
    ];
    protected $casts = [
        //'email_verified_at' => 'datetime',
    ];
}
