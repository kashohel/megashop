<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'base_product_id', 'code', 'variant', 'sub_variant', 'cost', 'price', 'quantity', 'alert_quantity', 'unit_id', 'purchase_unit', 'pos_unit', 'shop_unit',
        'tax', 'tax_type', 'shop_view', 'pos_view', 'expiry', 'expiry_day', 'file', 'link', 'length', 'width', 'height', 'weight', 'specification', 'image'
    ];

    protected $hidden = [
        //'password', 'remember_token',
    ];
    protected $casts = [
        //'email_verified_at' => 'datetime',
    ];
}
