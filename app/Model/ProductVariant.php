<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductVariant extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'name', 'parent_id'
    ];

    protected $hidden = [
        //'password', 'remember_token',
    ];
    protected $casts = [
        //'email_verified_at' => 'datetime',
    ];
}
