<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PriceGroup extends Model
{
    use SoftDeletes;
    protected $fillable = ['code', 'name'];

    public function products()
    {
        return $this->hasMany('App\Model\GroupPrice', 'price_group_id');
    }
}
