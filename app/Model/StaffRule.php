<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class StaffRule extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'code', 'name', 'details', 'products_index', 'products_add', 'products_edit', 'products_delete', 'products_cost', 'products_price', 'quotes_index', 'quotes_add', 'quotes_edit', 'quotes_pdf', 'quotes_email', 'quotes_delete', 'sales_index', 'sales_add', 'sales_pdf', 'sales_email', 'sales_delete', 'purchases_index', 'purchases_add', 'purchases_edit', 'purchases_pdf', 'purchases_email', 'purchases_delete', 'transfers_index', 'transfers_add', 'transfers_edit', 'transfers_pdf', 'transfers_email', 'transfers_delete', 'customers_index', 'customers_add', 'customers_edit', 'customers_delete', 'suppliers_index', 'suppliers_add', 'suppliers_edit', 'suppliers_delete', 'sales_deliveries', 'sales_add_delivery', 'sales_edit_delivery', 'sales_delete_delivery', 'sales_email_delivery', 'sales_pdf_delivery', 'sales_gift_cards', 'sales_add_gift_card', 'sales_edit_gift_card', 'sales_delete_gift_card', 'pos_index', 'sales_return_sales', 'reports_index', 'reports_warehouse_stock', 'reports_quantity_alerts', 'reports_expiry_alerts', 'reports_products', 'reports_daily_sales', 'reports_monthly_sales', 'reports_sales', 'reports_payments', 'reports_purchases', 'reports_profit_loss', 'reports_customers', 'reports_suppliers', 'reports_staff', 'reports_register', 'sales_payments', 'purchases_payments', 'purchases_expenses', 'products_adjustments', 'bulk_actions', 'customers_deposits', 'customers_delete_deposit', 'products_barcode', 'purchases_return_purchases', 'reports_expenses', 'reports_daily_purchases', 'reports_monthly_purchases', 'products_stock_count', 'edit_price',
        'show_cost', 'show_price', 'award_points', 'view_right', 'edit_right', 'allow_discount'
    ];

    public function assigned()
    {
        return $this->hasMany('App\Model\Staff', 'rule_id', 'id');
    }

    public function active()
    {
        return $this->hasMany('App\Model\Staff', 'rule_id', 'id')->where('active', 1);
    }

    public function access()
    {
        return $this->hasMany('App\Model\Staff', 'rule_id', 'id')->whereNotNull('username');
    }
}
// TODO: Make second line of column in migration and make name more meaningful and organized
