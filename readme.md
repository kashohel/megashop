<p align="center"><img src="https://laravel.com/assets/img/components/logo-laravel.svg"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

## Mega Shop

Mega Shop is modern super shop management system is one of the most advance software to manage a business following by standard way. Flexible and more customizable feature available to fit with any king of business. Some advance feature are given below :

- [Multiple unit](https://sokriyo.com), multi language and multi currency.
- Flexible and most customizable configuration.
- Product inventory with damage and expire product
- Multiple warehouse system with transfer product
- Customer list and group wise discount assign
- Sale in editable price with discount and tax
- Multiple user role and manage custom permission
- Each user have own activity log and report
- Total income expense and loyalty asset report
- Staff salary and and expense notification
- Bank, liquid and due cash report and manage
- Point of sale with two display mode
- Cash drawer management and receipt print on sale
- A lot of report and print out and export ready

With a lot of basic feature of inventory and POS system included here.

## Customer Priority

We try to build this system with global feature which can cover a large area of your requirement. We believe that we can fulfill your requirement. So feel free to share your needs and we will customize this for you.

If your requirement is out of this running system or fully deference things, we can be your satisfaction point to bring dream project live. Always we focus on customer satisfaction and keep eye on clients demand. We enjoy our job with something innovative and new.

## Requirement

This is fully online system so it can access and use from anywhere and anytime withing internet through. You must need internet accessible device with browsing ability.

## Security & Protection

This software update is ongoing to ensure safety. We use latest firewall and security filtering with DDoS protection to keep your data safe.

## License

By following [Terms & Condition](http://sokriyo.com/term) any user can deed with us to have service for a period of time and can be extend license. Pricing depending on duration and features.
